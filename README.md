# inx-replaceinkex

Explore options to migrate custom Inkscape extensions away from
`inkex.py` and other shared python modules distributed with Inkscape.

## Background

The unit conversion methods provided with the upstream `inkex.Effect()`
class always had a limited scope and introduced a major breakage of
external extensions with Inkscape 0.91. Now that core Inkscape has
vastly improved support for viewBox and document units/scale (Inkscape ≥
0.92), these unit conversions available in python-based extensions are
seriously broken in any document with a custom document scale defined
via viewBox attribute.

Upstream has been unable to even acknowlege the brokenness of
`unittouu()` and `uutounit()`, let alone shown efforts to fix it.
Instead core developers now patch workarounds into selected extensions
they maintain themselves, leaving users of other extensions and authors
of custom extensions based on `inkex.Effect()` stuck with broken
unit-related features.

## Options

There is no ideal solution for this (apart from upstream fixing the
broken methods instead of committing workarounds elsewhere which hide
the brokenness and at the same time perpetuate it). Authors of custom
extensions can
* patch every custom use case of the unit conversion methods that may
  expose the upstream breakage with yet another narrowly scoped
  workaround to calculate the document scale accurately, or
* embed overloading unit conversion methods with each extension, or
* ship a modified copy of inkex.py with each extension (embedded or as
  separate file) with fixed unit conversion methods, or
* entirely rewrite custom extensions to no longer sub-class
  `inkex.Effect()` and instead implement the necessary infrastructure in
  custom new-style base class(es), possibly along with Python 3
  compatibilty and migration from deprecated optparse to argparse.

Each option will either
* increase the maintainance burden for authors of custom extensions
  (every extension contains a copy of the chosen solution), or
* require users of such custom extensions to accept a more complicated
  installation process to retrieve a shared replacement for `inkex.py`
  from a separate repository.

## Related links

TODO
