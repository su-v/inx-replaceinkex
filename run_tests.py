#!/usr/bin/env python
"""
Run unit tests for inkex_local
"""
# standard library
import sys
import unittest

# local modules
from tests import test_inkex_local_basic
from tests import test_inkex_local_docscale
from tests import test_inkex_local_functions
from tests import test_inkex_local_units
from tests import test_inkex_upstream_docscale
from tests import test_inkex_upstream_local


MODULES = [module for name, module in sorted(sys.modules.items())
           if name.startswith('tests.test_inkex')]


def run_all(verbosity=0):
    """Run all tests as a single large group."""
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    for module in MODULES:
        suite.addTests(loader.loadTestsFromModule(module))
    unittest.TextTestRunner(verbosity=verbosity).run(suite)


def run_grouped_per_file(verbosity=0):
    """Run all tests per file imported from 'tests' module."""
    for module in MODULES:
        print(module.__name__)
        suite = unittest.TestLoader().loadTestsFromModule(module)
        unittest.TextTestRunner(verbosity=verbosity).run(suite)
        print('')


def run_grouped_per_class(verbosity=0):
    """NYI: Run tests per TestCase class per file in 'tests' module."""
    pass


def run_tests(run_mode='all', verbosity=0):
    """Run tests."""
    if run_mode.startswith('all'):
        run_all(verbosity=verbosity)
    elif run_mode.startswith('file'):
        run_grouped_per_file(verbosity=verbosity)
    elif run_mode.startswith('class'):
        run_grouped_per_class(verbosity=verbosity)


if __name__ == '__main__':
    run_tests(run_mode=sys.argv[-1] if len(sys.argv) > 1 else 'all')

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
