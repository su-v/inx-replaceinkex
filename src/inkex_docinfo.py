#!/usr/bin/env python
"""
inkex_docinfo.py - debug info about document scale and units.

Copyright (C) 2017 su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# standard library
import sys

# inkscape library
import simplestyle

# inkex replacement
try:
    import inkex_local as inkex
    # inkex.debug("inkex_local loaded.")
    from inkex_local import EffectUnits
    # inkex.debug("EffectUnits loaded from inkex_local.")
except ImportError:
    import inkex
    # inkex.debug("inkex loaded")
    try:
        from inkex import EffectUnits
        # inkex.debug("EffectUnits loaded from inkex.")
    except ImportError:
        inkex.debug("This extension requires inkex_local.py.")
        sys.exit(1)


def _ff(value, precision=8):
    """Pretty-printer for floats, return formatted string."""
    fstring = '.{0}f'.format(precision)
    return format(value, fstring).rstrip('0').rstrip('.')


def prpr(alist, precision=3):
    """Tiny pretty-printer for list of coords."""
    start = "("
    end = ")"
    sep = ", "
    result = start
    for num in alist:
        result += _ff(num, precision) + sep
    result = result[:-len(sep)] + end
    return result


def store_output(text=None, line=None):
    """Store output text (just to experiment with closures)."""
    if text is None:
        text = []

    def add(line=line):
        """Append line to local text list, else return list."""
        if line is not None:
            text.append(line)
        else:
            return text

    return add


def add_text(x=None, y=None, style=None):
    """Return text object <text/>."""
    text = inkex.etree.Element(inkex.addNS('text', 'svg'))
    text.set(inkex.addNS('space', 'xml'), "preserve")
    if style:
        text.set('style', simplestyle.formatStyle(style))
    if x:
        text.set('x', str(x))
    if y:
        text.set('y', str(y))
    return text


def add_line(line, x=None, y=None, dx=None, dy=None):
    """Return line wrapped in <tspan> element."""
    # pylint: disable=invalid-name
    tspan = inkex.etree.Element(inkex.addNS('tspan', 'svg'))
    tspan.set(inkex.addNS('role', 'sodipodi'), "line")
    if x:
        tspan.set('x', str(x))
    if dx:
        tspan.set('dx', str(dx))
    if y:
        tspan.set('y', str(y))
    if dy:
        tspan.set('dy', str(dy))
    tspan.text = str(line)
    return tspan


class EffectUnitsInfo(EffectUnits):
    """Display debug information about current document."""

    def __init__(self):
        """Init base class."""
        super(EffectUnitsInfo, self).__init__()

        # instance attributes
        self.add = None

    # Debug info
    # ==========
    # output helper methods

    def create_text(self, base=None, position=None, layer=None):
        """Configure and create new text object."""

        # base size (text, anchor offset)
        base = 12 if base is None else base
        # text anchor position relative to page area
        #   position: origin | right | center
        position = 'origin' if position is None else position
        # container element to append text to
        #   layer: new | current | None
        layer = 'current' if layer is None else layer

        # font properties
        font_size = self.inx_unit_to_uu("{}px".format(base))
        font_family = "monospace"
        # baseline spacing
        line_height = 1.25
        # text style
        sdict = {}
        sdict['font-size'] = font_size
        sdict['font-family'] = font_family
        sdict['line-height'] = line_height
        # text position
        if position == 'origin':
            x, y = inkex.mat_apply_to_point(
                self.mat_desktop_to_uu(),
                [0 + font_size, self.inx_get_viewport_prop('height')])
        elif position == 'right':
            x, y = inkex.mat_apply_to_point(
                self.mat_desktop_to_uu(),
                [self.inx_get_viewport_prop('width') + font_size,
                 self.inx_get_viewport_prop('height')])
        elif position == 'center':
            x, y = self.inx_get_display_prop('center')
        else:
            x, y = [0.0, 0.0]
        # create text
        text = add_text(x=x, y=y, style=sdict)
        # spacer line if text anchor at top of page
        if position in ['origin', 'right']:
            text.append(add_line(" "))
        # add to document
        if layer == 'new':
            parent = inkex.etree.Element(inkex.add_ns('g', 'svg'))
            parent.set(inkex.add_ns('groupmode', 'inkscape'), 'layer')
            parent.set(inkex.add_ns('label', 'inkscape'), 'docinfo')
            self.inx_get_root().append(parent)
        elif layer == 'current' and hasattr(self, 'current_layer'):
            parent = self.current_layer
        else:
            parent = self.inx_get_root()
        parent.append(text)
        # return new object
        return text

    def write_stderr(self, title, output):
        """Create text with output and write to stderr."""
        # pylint: disable=no-self-use
        # include title string as header + separator
        header = 'ruler' if title is not None else None
        # add title string
        if header:
            inkex.errormsg(str(title))
            if header == 'blank':
                inkex.errormsg('')
            elif header == 'ruler':
                inkex.errormsg(len(title) * "=")
        # return output                     # output is a list of strings
        # inkex.debug(type(output))
        for line in output:
            inkex.errormsg(line)            # errormsg() accepts single string

    def write_canvas(self, title, output):
        """Create text with output and add as SVG text to canvas."""
        # pylint: disable=no-self-use
        # include title string as header + separator
        header = 'ruler' if title is not None else None
        # create outer text object
        text = self.create_text(position='right', layer='new')
        # add input string
        if header:
            text.append(add_line(title))
            if header == 'blank':
                text.append(add_line(" "))
            elif header == 'ruler':
                text.append(add_line(len(title) * "="))
        # process output                    # output is a list of strings
        # inkex.debug(type(output))
        for line in output:
            text.append(add_line(line))     # add_line() applies str(line)

    def write_clipboard(self, title, output):
        """Create text with output and send to clipboard."""
        # pylint: disable=no-self-use,unused-argument
        inkex.errormsg("Output to clipboard is not yet implemented.")

    def write_file(self, title, output):
        """Create text with output and write to external file."""
        # pylint: disable=no-self-use,unused-argument
        inkex.errormsg("Output to file is not yet implemented.")

    def show(self, title=None, output=None, target='stderr'):
        """Output to requested targets."""
        if title is None and output is None:
            return
        if 'stderr' in target:
            result = self.write_stderr(title, output)
        if 'canvas' in target:
            result = self.write_canvas(title, output)
        if 'clipboard' in target:
            result = self.write_clipboard(title, output)
        if 'file' in target:
            result = self.write_file(title, output)
        return result

    # Debug info
    # ==========
    # methods to gather information

    def _info_functions(self, root):
        """Information about functions provided in inkex_local."""
        # pylint: disable=no-self-use

        self.add('inkex functions:')
        self.add('================')

        self.add('svg geometry: ' + str(
            inkex.svg_geometry(root)))

        self.add('valid viewbox: ' + str(
            inkex.get_viewbox(root)))

        self.add('viewbox geometry: ' + str(
            inkex.viewbox_geometry(root)))

        self.add('viewport geometry: ' + str(
            inkex.viewport_geometry(root)))

        self.add('apply aspectration: ' + str(
            inkex.apply_aspectratio(root)))

        self.add('compute viewport: ' + str(
            inkex.compute_viewport(root)))

        self.add('compute viewport geom: ' + str(
            inkex.compute_viewport_geom(root)))

        self.add('compute viewport size: ' + str(
            inkex.compute_viewport_size(root)))

        self.add('compute viewport scale: ' + str(
            inkex.compute_viewport_scale(root)))

        self.add('root scale: ' + str(
            inkex.get_root_scale(root)))

        self.add('root offset: ' + str(
            inkex.get_root_offset(root)))

        self.add('root size: ' + str(
            inkex.get_root_size(root)))

        self.add('')

    def _info_methods(self):
        """Information about methods provided in EffectUnits()."""

        display_units = self.inx_get_display_prop("unit")
        precision = 3

        self.add('inkex.EffectUnits() methods:')
        self.add('============================')

        self.add('document scale: ' + str(
            self.get_document_scale()))

        self.add('')

        self.add('matrix [uu to unit] (display units): ' + str(
            self.mat_uu_to_unit(display_units)))

        self.add('matrix [unit to uu] (display units): ' + str(
            self.mat_unit_to_uu(display_units)))

        self.add('matrix [unit to unit] (inch, px): ' + str(
            self.mat_unit_to_unit('in', 'px')))

        self.add('')

        self.add('matrix [uu to desktop]: ' + str(
            self.mat_uu_to_desktop()))

        self.add('matrix [desktop to uu]: ' + str(
            self.mat_desktop_to_uu()))

        self.add('')

        self.add('unit to uu (10pt): ' + _ff(
            self.inx_unit_to_uu("10pt"), precision))

        self.add('uu to unit (10.0, pt): ' + _ff(
            self.inx_uu_to_unit(10.0, 'pt'), precision))

        self.add('unit to unit (Inch -> CSS px): ' + _ff(
            self.inx_unit_to_unit(1.0, 'in', 'px'), precision))

        self.add('')

        self.add('page size: ' + str(
            self.inx_get_page_prop("size")))

        self.add('page width: ' + str(
            self.inx_get_page_prop("width")))

        self.add('page height: ' + str(
            self.inx_get_page_prop("height")))

        self.add('page units: ' + str(
            self.inx_get_page_prop("units")))

        self.add('')

        self.add('viewport size (user units): ' + str(
            self.inx_get_viewport_prop("size")))

        self.add('viewport width (user units): ' + str(
            self.inx_get_viewport_prop("width")))

        self.add('viewport height (user units): ' + str(
            self.inx_get_viewport_prop("height")))

        self.add('viewport computed width (CSS px): ' + str(
            self.inx_get_viewbox_prop("computed_width", "px")))

        self.add('viewport computed height (CSS px): ' + str(
            self.inx_get_viewbox_prop("computed_height", "px")))

        self.add('viewport rect (user units, coords): ' + str(
            self.inx_get_viewport_prop("rect")))

        self.add('viewport center (user units, coords): ' + str(
            self.inx_get_viewport_prop("center")))

        self.add('viewport scale: ' + str(
            self.inx_get_viewport_prop("scale")))

        self.add('')

        self.add('viewbox size: ' + str(
            self.inx_get_viewbox_prop("size")))

        self.add('viewbox width (user units): ' + str(
            self.inx_get_viewbox_prop("width")))

        self.add('viewbox height (user units): ' + str(
            self.inx_get_viewbox_prop("height")))

        self.add('viewbox computed width (CSS px): ' + str(
            self.inx_get_viewbox_prop("computed_width")))

        self.add('viewbox computed height (CSS px): ' + str(
            self.inx_get_viewbox_prop("computed_height")))

        self.add('viewbox rect (user coords, units): ' + str(
            self.inx_get_viewbox_prop("rect", "uu")))

        self.add('viewbox rect (desktop coords, display units): ' + str(
            self.inx_get_viewbox_prop("rect", "display")))

        self.add('viewbox rect (desktop coords, cm): ' + str(
            self.inx_get_viewbox_prop("rect", "cm")))

        self.add('viewbox / document unit: ' + str(
            self.inx_get_viewbox_prop("unit")))

        self.add('viewbox / document scale: ' + str(
            self.inx_get_viewbox_prop("scale")))

        self.add('')
        self.add('display units: ' + str(
            self.inx_get_display_prop("unit")))

        self.add('display view center: ' + str(
            self.inx_get_display_prop("center")))

        self.add('')

        self.add('inkscape version (runtime): ' + str(
            self.inx_inkscape_version("runtime")))

        self.add('inkscape version (document): ' + str(
            self.inx_inkscape_version("document")))

        self.add('')

    def _info_instance_attrib(self):
        """Information about instance attributes of EffectUnits()."""

        self.add('inkex.EffectUnits() instance attributes:')
        self.add('========================================')

        self.add('cached scale: ' + str(
            self.cached_scale))

        self.add('inkruntime: ' + str(
            self.inkruntime))

        self.add('inkversion: ' + str(
            self.inkversion))

        self.add('legacy: ' + str(
            self.legacy))

        self.add('')

    def _info_computed_values(self, root):
        """Information about computed values related to viewport, viewbox."""

        vb_offset, vb_size = inkex.viewbox_geometry(root)
        vp_offset, vp_size = inkex.compute_viewport_geom(root)

        css_px_units = "px"
        display_units = self.inx_get_display_prop("unit")

        dpi_self = int(round(self.inx_unit_to_unit(1.0, 'in', 'px')))

        self.add('Computed viewport and viewBox:')
        self.add('==============================')

        self.add('In user units:')

        self.add('viewport size: ' + prpr(
            vp_size))

        self.add('viewport offset: ' + prpr(
            vp_offset))

        self.add('viewbox size: ' + prpr(
            vb_size))

        self.add('viewbox offset: ' + prpr(
            vb_offset))

        self.add('Viewport center in SVG coords: ' + prpr(
            self.inx_get_viewport_prop("center")))

        self.add('Desktop origin in SVG coords: ' + prpr(
            inkex.mat_apply_to_point(self.mat_desktop_to_uu(), [0, 0])))

        self.add('SVG origin in desktop coords: ' + prpr(
            inkex.mat_apply_to_point(self.mat_uu_to_desktop(), [0, 0])))

        self.add('')

        self.add("In CSS px ({}dpi):".format(dpi_self))

        self.add('viewport size: ' + prpr(
            (self.inx_uu_to_unit(vp_size[0], css_px_units),
             self.inx_uu_to_unit(vp_size[1], css_px_units))))

        self.add('viewport offset: ' + prpr(
            (self.inx_uu_to_unit(vp_offset[0], css_px_units),
             self.inx_uu_to_unit(vp_offset[1], css_px_units))))

        self.add('viewbox size: ' + prpr(
            (self.inx_uu_to_unit(vb_size[0], css_px_units),
             self.inx_uu_to_unit(vb_size[1], css_px_units))))

        self.add('viewbox offset: ' + prpr(
            (self.inx_uu_to_unit(vb_offset[0], css_px_units),
             self.inx_uu_to_unit(vb_offset[1], css_px_units))))

        self.add('Viewport center in SVG coords: ' + prpr(
            inkex.mat_apply_to_point(
                self.mat_uu_to_unit(css_px_units),
                self.inx_get_viewport_prop("center"))))

        self.add('Desktop origin in SVG coords: ' + prpr(
            inkex.mat_apply_to_point(
                self.mat_uu_to_unit(css_px_units),
                inkex.mat_apply_to_point(self.mat_desktop_to_uu(), [0, 0]))))

        self.add('SVG origin in desktop coords: ' + prpr(
            inkex.mat_apply_to_point(
                self.mat_uu_to_unit(css_px_units),
                inkex.mat_apply_to_point(self.mat_uu_to_desktop(), [0, 0]))))

        self.add('')

        self.add("In display units ({}, {}dpi):".format(display_units,
                                                        dpi_self))

        self.add('viewport size: ' + prpr(
            (self.inx_uu_to_unit(vp_size[0], display_units),
             self.inx_uu_to_unit(vp_size[1], display_units))))

        self.add('viewport offset: ' + prpr(
            (self.inx_uu_to_unit(vp_offset[0], display_units),
             self.inx_uu_to_unit(vp_offset[1], display_units))))

        self.add('viewbox size: ' + prpr(
            (self.inx_uu_to_unit(vb_size[0], display_units),
             self.inx_uu_to_unit(vb_size[1], display_units))))

        self.add('viewbox offset: ' + prpr(
            (self.inx_uu_to_unit(vb_offset[0], display_units),
             self.inx_uu_to_unit(vb_offset[1], display_units))))

        self.add('Viewport center in SVG coords: ' + prpr(
            inkex.mat_apply_to_point(
                self.mat_uu_to_unit(display_units),
                self.inx_get_viewport_prop("center"))))

        self.add('Desktop origin in SVG coords: ' + prpr(
            inkex.mat_apply_to_point(
                self.mat_uu_to_unit(display_units),
                inkex.mat_apply_to_point(self.mat_desktop_to_uu(), [0, 0]))))

        self.add('SVG origin in desktop coords: ' + prpr(
            inkex.mat_apply_to_point(
                self.mat_uu_to_unit(display_units),
                inkex.mat_apply_to_point(self.mat_uu_to_desktop(), [0, 0]))))

        self.add('')

    # Debug info
    # ==========
    # Display output of functions, methods and instance attributes

    def inx_effect_units_info(self, target='stderr'):
        """Information about computed size and scale."""
        # TODO: add options to INX, OptionParser
        if target is None:
            target = 'canvas'
        # prepare text storage (closure)
        self.add = store_output()
        # root node
        root = self.inx_get_root()
        # functions
        self._info_functions(root)
        # methods
        self._info_methods()
        # instance attributes
        self._info_instance_attrib()
        # computed values
        self._info_computed_values(root)
        # output info to target
        return self.show(output=self.add(), target=target)

    # Overload base methods
    # =====================
    # used to process the current document of the class instance

    def inx_main(self):
        """Output information about document scale and units."""
        # pylint: disable=unused-variable
        result = self.inx_effect_units_info(target=None)


if __name__ == '__main__':
    ME = EffectUnitsInfo()
    ME.inx_process()


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
