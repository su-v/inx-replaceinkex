#!/usr/bin/env python
"""
inkex_local - rewrite Inkscape's inkex.py module for custom extensions:
              * test python 2/3 compatibility
              * add extended document scale support
              * split Effect into sub-classes

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-lines

# standard library
import copy
import gettext
import locale
import math  # cos, sin, radians
import numbers
import optparse  # TODO: replace with argparse
import os
import random
import re
import sys

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
from contextlib import contextmanager

# explicit imports from third-party modules
from lxml import etree

# local library
try:
    import inkex as inkex_orig
except ImportError:
    inkex_orig = None


__version__ = '0.0'


# ----------------------------------------------------------------------------
# translation
# ----------------------------------------------------------------------------

def translite():
    """Configure and install a gettext Translations instance."""

    _domain = 'inkscape'
    _dir = None
    _lang = []

    if 'INKSCAPE_LOCALEDIR' in os.environ:
        _dir = os.environ['INKSCAPE_LOCALEDIR']
    elif 'PACKAGE_LOCALE_DIR' in os.environ:
        _dir = os.environ['PACKAGE_LOCALE_DIR']

    if sys.platform.startswith('win'):
        if 'LANG' in os.environ:
            current_locale = os.environ['LANG']
        elif 'LC_ALL' in os.environ:
            current_locale = os.environ['LC_ALL']
        else:
            current_locale = locale.getdefaultlocale()[0]
            os.environ['LANG'] = current_locale
        _lang = [current_locale]

    if _dir is not None:
        if len(_lang):
            trans = gettext.translation(_domain, _dir, _lang, fallback=True)
        else:
            trans = gettext.translation(_domain, _dir, fallback=True)
    else:
        trans = gettext.translation(_domain, fallback=True)

    trans.install()


# ----------------------------------------------------------------------------
# Exceptions
# ----------------------------------------------------------------------------

class InvalidLengthString(ValueError):
    """Invalid argument passed to parse_length()."""
    pass


class UnsupportedUnitForLength(ValueError):
    """Unsupported unit identifier."""
    pass


# ----------------------------------------------------------------------------
# InkOption() - TODO: migrate to argparse()
# ----------------------------------------------------------------------------

def check_inkbool(option, opt, value):
    """Add type_checker function for optparse instance."""
    # pylint: disable=unused-argument
    if str(value).capitalize() == 'True':
        return True
    elif str(value).capitalize() == 'False':
        return False
    else:
        raise optparse.OptionValueError(
            "option {0}: invalid inkbool value: {1}".format(opt, value))


class InkOption(optparse.Option):
    """Sub-class of optparse.Option for Inkscape's custom boolean option."""
    TYPES = optparse.Option.TYPES + ("inkbool",)
    TYPE_CHECKER = copy.copy(optparse.Option.TYPE_CHECKER)
    TYPE_CHECKER["inkbool"] = check_inkbool


# ----------------------------------------------------------------------------
# EffectBase(): constants, functions
# ----------------------------------------------------------------------------

VERBOSE = False
NSS = {
    u'sodipodi': u'http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd',
    u'cc':       u'http://creativecommons.org/ns#',
    u'ccOLD':    u'http://web.resource.org/cc/',
    u'svg':      u'http://www.w3.org/2000/svg',
    u'dc':       u'http://purl.org/dc/elements/1.1/',
    u'rdf':      u'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    u'inkscape': u'http://www.inkscape.org/namespaces/inkscape',
    u'xlink':    u'http://www.w3.org/1999/xlink',
    u'xml':      u'http://www.w3.org/XML/1998/namespace',
    u'osb':      u'http://www.openswatchbook.org/uri/2009/osb',
}


def _ff(value, precision=8):
    """Pretty-printer for floats, return formatted string."""
    fstring = '.{0}f'.format(precision)
    return format(value, fstring).rstrip('0').rstrip('.')


def add_ns(tag, namespace=None):
    """Prepend namespace to XML tag name."""
    if namespace in NSS and (tag and not tag.startswith('{')):
        return "{{{0}}}{1}".format(NSS[namespace], tag)
    else:
        return tag


def showme(what):
    """Verbose output based on global variable."""
    if VERBOSE:
        sys.stderr.write(str(what) + '\n')


def debug(what):
    """Debug output (any object type)."""
    if sys.version_info < (3,):
        sys.stderr.write(str(what) + '\n')
    else:
        if isinstance(what, str):
            # sys.stderr.write(what + '\n')
            sys.stderr.write(str(what) + '\n')
        elif isinstance(what, bytes):
            # sys.stderr.write(what.decode() + '\n')
            sys.stderr.write(str(what) + '\n')
            # sys.stderr.write(repr(what) + '\n')
        else:
            sys.stderr.write(str(what) + '\n')
    return what


def errormsg(msg):
    """Intended for end-user-visible error messages (strings)."""
    if sys.version_info < (3,):
        # pylint: disable=undefined-variable
        if isinstance(msg, unicode):
            sys.stderr.write(msg.encode('utf-8') + '\n')
        else:
            msg = unicode(msg, 'utf-8', errors='replace')
            sys.stderr.write(msg.encode('utf-8') + '\n')
    else:
        if isinstance(msg, str):
            # msg is unicode text (decoded)
            sys.stderr.write(msg + '\n')
        elif isinstance(msg, bytes):
            # msg is bytes (encoded)
            sys.stderr.write(msg.decode() + '\n')
        else:
            debug(msg)


# ----------------------------------------------------------------------------
# EffectBase(): class, methods
# ----------------------------------------------------------------------------

class EffectBase(object):
    """Base class providing SVG parsing and output for extension scripts."""

    def __init__(self, *args, **kwargs):
        """Init base class."""
        # pylint: disable=unused-argument
        showme("EffectBase.__init__()")
        super(EffectBase, self).__init__()

        # instance attributes
        self.svg_file = None
        self.document = None
        self.original_document = None
        self.stderr = StringIO()

    @contextmanager
    def inx_redirect_stderr(self, new_target=None):
        """Temporarily redirect stderr to buffer."""
        if new_target is None:
            new_target = self.stderr
        old_target, sys.stderr = sys.stderr, new_target
        try:
            yield new_target
        finally:
            sys.stderr = old_target
            self.inx_restore_stderr()

    def inx_has_clipboard(self, root=None):
        """Check presence of clipboard tag in current document."""
        try:
            root = self.document.getroot() if root is None else root
        except AttributeError:
            return False
        clipboard = root.find(add_ns('clipboard', 'inkscape'))
        return True if clipboard is not None else False

    def inx_restore_stderr(self):
        """Restore redirected stderr if not clipboard snippet."""
        if not self.inx_has_clipboard():
            sys.stderr.write(self.stderr.getvalue())

    def inx_parse_svg(self, filename=None):
        """Parse document in specified file or on stdin."""
        stream = None
        if filename is not None:
            # First try to open the file from the function argument
            try:
                stream = open(filename, 'rb')
            except IOError:
                errormsg("Unable to open specified file: " +
                         "{}".format(filename))
                # sys.exit()
        elif self.svg_file is not None:
            # If it wasn't specified, try to open the file specified as
            # an object member
            try:
                stream = open(self.svg_file, 'rb')
            except IOError:
                errormsg("Unable to open object member file: " +
                         "{}".format(self.svg_file))
                # sys.exit()
        else:
            # Finally, if the filename was not specified anywhere, use
            # standard input stream
            stream = sys.stdin

        if stream is not None:
            xml_parser = etree.XMLParser(huge_tree=True)
            self.document = etree.parse(stream, parser=xml_parser)
            self.original_document = copy.deepcopy(self.document)
            stream.close()

    def inx_main(self):
        """Method to apply changes to the document."""
        pass

    def inx_is_modified(self, doc=None, ignore=False):
        """Check modified status of doc."""
        doc = self.document if doc is None else doc
        if not ignore:
            original = etree.tostring(self.original_document)
            result = etree.tostring(doc)
            return original != result
        else:
            return True

    def inx_output(self):
        """Serialize document into XML on stdout"""
        if self.document is not None and self.inx_is_modified():
            # pylint: disable=no-member
            if sys.version_info < (3,):
                self.document.write(sys.stdout)
            else:
                self.document.write(sys.stdout.buffer)

    def inx_process(self, args=sys.argv[1:], output=True):
        """Affect an SVG document with a callback effect"""
        with self.inx_redirect_stderr():
            self.svg_file = args[-1]
            translite()
            self.inx_parse_svg()
            self.inx_main()
        if output:
            self.inx_output()


# ----------------------------------------------------------------------------
# EffectUtil(): class, methods
# ----------------------------------------------------------------------------

class EffectUtil(EffectBase):
    """EffectBase sub-class prodviding utility and helper methods."""

    def __init__(self):
        """Init base class."""
        showme("EffectUtil.__init__()")
        super(EffectUtil, self).__init__()

        # instance attributes
        self.doc_ids = {}
        self.current_layer = None

    def inx_collect_ids(self, doc=None):
        """Update self.doc_ids with id values in current document."""
        doc = self.document if doc is None else doc
        for node in doc.getroot().iter(tag=etree.Element):
            if 'id' in node.attrib:
                node_id = node.get('id')
                self.doc_ids[node_id] = 1

    def inx_unique_id(self, old_id, make_new_id=True):
        """Create unique id."""
        string = ('0123456789' +
                  'abcdefghijklmnopqrstuvwxyz' +
                  'ABCDEFGHIJKLMNOPQRSTUVWXYZ')
        new_id = old_id
        if make_new_id:
            while new_id in self.doc_ids:
                new_id += random.choice(string)
            self.doc_ids[new_id] = 1
        return new_id

    def inx_get_element_by_id(self, val, doc=None):
        """Return element with id *val* or None."""
        doc = self.document if doc is None else doc
        path = "//*[@id=\"{0}\"]".format(val)
        result = doc.xpath(path, namespaces=NSS)
        if result:
            return result[0]
        else:
            return None

    def inx_get_element_by_xpath(self, path, doc=None):
        """Return node matching xpath expression *path*."""
        doc = self.document if doc is None else doc
        result = doc.xpath(path, namespaces=NSS)
        if result and len(result):
            return result[0]
        else:
            return None

    def inx_get_parent(self, node, doc=None):
        """Return parent of *node*."""
        doc = self.document if doc is None else doc
        for parent in doc.getiterator():
            if node in parent.getchildren():
                return parent

    def inx_get_namedview(self, doc=None):
        """Return <namedview/> document node in sodipodi namespace."""
        doc = self.document if doc is None else doc
        path = '//sodipodi:namedview'
        result = doc.xpath(path, namespaces=NSS)
        if result:
            return result[0]
        else:
            return None

    def inx_create_guide(self, pos_x, pos_y, angle):
        """Create a new guide based on x, y and angle."""
        attribs = {
            'position': "{},{}".format(str(pos_x), str(pos_y)),
            'orientation': "{},{}".format(str(math.sin(math.radians(angle))),
                                          str(-math.cos(math.radians(angle)))),
        }
        guide = etree.Element(add_ns('guide', 'sodipodi'), attribs)
        namedview = self.inx_get_namedview()
        namedview.append(guide)
        return guide

    def inx_get_current_layer(self):
        """Get node of current layer as defined in <namedview/>."""
        self.current_layer = self.document.getroot()
        namedview = self.inx_get_namedview()
        if namedview is not None:
            layer_id = namedview.get(add_ns('current-layer', 'inkscape'), None)
            if layer_id is not None:
                path = "//svg:g[@id=\"{0}\"]".format(layer_id)
                layer = self.inx_get_element_by_xpath(path)
                if layer is not None:
                    self.current_layer = layer
        return self.current_layer

    def inx_main(self):
        """Method to apply changes to the document."""
        pass

    def inx_process(self, args=sys.argv[1:], output=True):
        """Affect an SVG document with a callback effect"""
        with self.inx_redirect_stderr():
            self.svg_file = args[-1]
            translite()
            self.inx_parse_svg()
            self.inx_collect_ids()
            self.inx_get_current_layer()
            self.inx_main()
        if output:
            self.inx_output()


# ----------------------------------------------------------------------------
# EffectUnits(): constants, functions
# ----------------------------------------------------------------------------

# Maintain separate dicts for 90 and 96 dpi to allow extensions to
# implement support for legacy (or future) documents.

UUCONV_90DPI = {
    'in': 90.0,
    'pt': 1.25,
    'px': 1.0,
    'mm': 3.543307086614174,
    'cm': 35.43307086614174,
    'm':  3543.307086614174,
    'km': 3543307.086614174,
    'pc': 15.0,
    'yd': 3240.0,
    'ft': 1080.0
}
UUCONV_96DPI = {
    'in': 96.0,
    'pt': 1.333333333333333,
    'px': 1.0,
    'mm': 3.779527559055119,
    'cm': 37.79527559055119,
    'm':  3779.527559055119,
    'km': 3779527.559055119,
    'pc': 16.0,
    'yd': 3456.0,
    'ft': 1152.0
}

# Default dictionary of unit-to-user_unit conversion factors.

# Set overload_uuconv to True to replace unit table from start
OVERLOAD_UUCONV = False

# Set initial unit conversion table
UUCONV = UUCONV_96DPI

if not OVERLOAD_UUCONV:
    # To start with default unit table from inkex.py, and override it later
    # with inx_switch_uuconv() called in inx_main() after having guessed
    # runtime version based on inkex functions in
    # inx_inkscape_version_runtime().
    if inkex_orig is not None:  # original inkex was imported
        if hasattr(inkex_orig, 'uuconv'):
            # Inkscape 0.48 and earlier
            # pylint: disable=no-member
            UUCONV = inkex_orig.uuconv
        elif hasattr(inkex_orig.Effect, '_Effect__uuconv'):
            # Inkscape 0.91 and later
            # pylint: disable=no-member,protected-access
            UUCONV = inkex_orig.Effect._Effect__uuconv

# Notes on relative units:
#
# em, ex
# ======
# In almost every browser, 16px is the standard for proportional
# fonts.
# http://stackoverflow.com/a/29512572/2136422
#
# The initial value of font-size, which is used when calculating the
# default font size of html as well as calculating rem and em in
# media queries, is medium. The 16px comes from user preferences
# within the browser.
# http://stackoverflow.com/q/29511983/2136422
#
# The parent element of body is html, whose default font size
# matches the browser's default font size setting (typically 16px).
# The html element's default font size is the initial value, medium,
# which according to the spec corresponds to the preferred default
# font size as set by the user.
# http://stackoverflow.com/a/10470734/2136422
#
# Pixels:   16 px
# EMs:       1.000 em
# Percent: 100.0 %
# Points:   12 pt
# http://stackoverflow.com/a/10470749/2136422
#
# Inkscape itself "fails" with em, ex in width, height if viewBox is
# missing (the viewBox computes to "-1 -1 1 1").
#
# percent
# =======
# Initial value of width, height:
# If the attribute is not specified, the effect is as if a value of
# '100%' were specified.
#
# Percent is only used in SVG presentation attributes with <length>,
# not in CSS style properties:
#
# Note that the non-property <length> definition also allows a
# percentage unit identifier. The meaning of a percentage length
# value depends on the attribute for which the percentage length
# value has been specified. Two common cases are: (a) when a
# percentage length value represents a percentage of the viewport
# width or height (refer to the section that discusses units in
# general), and (b) when a percentage length value represents a
# percentage of the bounding box width or height on a given object
# (refer to the section that describes object bounding box units).
# https://www.w3.org/TR/SVG11/types.html#DataTypeLength
#
# Default width, height if not specified | auto?
# in browsers often:  300px x 150px
# https://css-tricks.com/scale-svg/
# https://www.w3.org/TR/CSS2/visudet.html#inline-replaced-width
#
# Inkscape itself "fails" with percent in width, height if viewBox
# is missing (the viewBox computes to "-1 -1 1 1").

# supported unit types
ABSOLUTE_UNITS = ('pt', 'pc', 'cm', 'mm', 'in')
PIXEL_UNITS = ('', 'px')
RELATIVE_UNITS = ('em', 'ex', '%')
# CSS / browser defaults
FONTSIZE_MEDIUM = (12, 'pt')
# WIDTH_FALLBACK = (300, 'px')        # web browsers
# HEIGHT_FALLBACK = (150, 'px')       # web browsers
WIDTH_FALLBACK = (100, 'px')        # inkscape
HEIGHT_FALLBACK = (100, 'px')       # inkscape
SIZE_FALLBACK = (WIDTH_FALLBACK, HEIGHT_FALLBACK)
# known strings in (property) values describing a length
FLOAT_STRINGS = ['nan', 'inf']
CSS_KEYWORDS = [
    # kerning
    'auto',
    # baseline-shift, font-size, kerning, letter-spacing,
    # stroke-dashoffset, stroke-width, word-spacing
    'inherit',
    # other CSS-wide value keywords
    'initial', 'unset',
    # baseline-shift
    'baseline', 'sub', 'super',
    # letter-spacing, word-spacing, line-height
    'normal',
    # font-size (absolute-size)
    'xx-small', 'x-small', 'small',
    'medium',
    'large', 'x-large', 'xx-large',
    # font-size (relative-size)
    'larger', 'smaller'
]


# ----- helper functions for SVG lengths and coordinates

def ident_mat():
    """Return 2x3 identity matrix."""
    return [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]


def is_float(val):
    """Test whether val is a float."""
    return isinstance(val, numbers.Real)


def is_int(val):
    """Test whether val is an integer."""
    return isinstance(val, numbers.Integral)


def is_real(val):
    """Check whether val is int or float."""
    return is_int(val) or is_float(val)


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    """Test approximate equality.

    ref:
        PEP 485 -- A Function for testing approximate equality
        https://www.python.org/dev/peps/pep-0485/#proposed-implementation
    """
    # pylint: disable=invalid-name
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def match_unit(val, eps=0.01):
    """Fuzzy matching of value to one of the known unit factors."""
    match = None
    for key in UUCONV:
        if isclose(UUCONV[key], val, rel_tol=eps):
            match = UUCONV[key]
    return match or val


def check_length_unit(unit, context='all'):
    """Check whether unit is supported for lengths."""
    units = []
    if context == 'uuconv':
        units.extend(UUCONV.keys())
    elif context == 'abs':
        units.extend(UUCONV.keys())
        units.extend(PIXEL_UNITS)
    elif context == 'rel':
        units.extend(RELATIVE_UNITS)
    elif context == 'css2':
        units.extend(ABSOLUTE_UNITS)
        units.extend(['px'])
        units.extend(RELATIVE_UNITS)
    elif context == 'svg':
        units.extend(ABSOLUTE_UNITS)
        units.extend(PIXEL_UNITS)
        units.extend(RELATIVE_UNITS)
    else:  # if context == 'all':
        units.extend(UUCONV.keys())
        units.extend(PIXEL_UNITS)
        units.extend(RELATIVE_UNITS)
    if context != 'svg':
        unit = unit.lower()
    return unit in units


def split_length(val, value=0.0, unit=''):
    """Split length string *val* into value, unit."""
    params = re.compile(
        r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)(.*$)')
    split_match = params.match(val)
    if split_match:
        split_vals = split_match.groups()
        if len(split_vals):
            value = float(split_vals[0])
        if len(split_vals) > 1:
            unit = split_vals[-1].strip()
    return value, unit


def parse_length(val, context='all'):
    """Parse length into list of float (value) and string (unit)."""
    # initial values
    value = 0.0
    unit = ''
    # check val
    if not isinstance(val, str):
        raise InvalidLengthString(
            "Invalid length value: '{}'".format(val))
    elif val in CSS_KEYWORDS:
        raise InvalidLengthString(
            "Unsupported property value: '{}'".format(val))
    elif len(val.split(',')) > 1:
        raise InvalidLengthString(
            "Invalid decimal separator: '{}'".format(val))
    # split
    try:
        value = float(val)
    except ValueError:
        value, unit = split_length(val)
    # check unit
    if not check_length_unit(unit, context):
        raise UnsupportedUnitForLength(
            "Unsupported length unit identifier '{}' ".format(unit) +
            "in context '{}'.".format(context))
    return (value, unit)


def print_length(alist):
    """Return length string."""
    return '{}{}'.format(*alist)


def parse_point(val):
    """Parse point string into list of floats."""
    # pylint: disable=bad-builtin
    return tuple(float(v) for v in filter(None, re.split("[, ]", val)))


def print_point(alist):
    """Return point list as string joined with comma."""
    return ','.join(str(v) for v in alist)


def parse_list_of_lengths(val):
    """Parse SVG list-of-lengths string into list of SVG lengths."""
    # pylint: disable=bad-builtin
    return tuple(parse_length(v, context='svg')
                 for v in filter(None, re.split("[, ]+", val)))


def print_list_of_lengths(alist):
    """Return list-of-lengths list as string joined with whitespace."""
    return ' '.join(print_length(l) for l in alist)


def parse_list_of_numbers(val):
    # pylint: disable=bad-builtin
    """Parse list-of-numbers string into list of floats."""
    return tuple(float(v) for v in filter(None, re.split("[, ]+", val)))


def print_list_of_numbers(alist):
    """Return list-of-numbers list as string joined with whitespace."""
    return ' '.join(str(v) for v in alist)


def has_attr(node, attr):
    """Check whether node has non-empty attribute *attr*."""
    return attr in node.attrib and len(node.get(attr, ''))


def has_x(node):
    """Check whether node has non-empty x attribute."""
    return has_attr(node, 'x')


def has_y(node):
    """Check whether node has non-empty y attribute."""
    return has_attr(node, 'y')


def has_width(node):
    """Check whether node has non-empty width attribute."""
    return has_attr(node, 'width')


def has_height(node):
    """Check whether node has non-empty height attribute."""
    return has_attr(node, 'height')


def has_viewbox(node):
    """Check whether node has non-empty viewBox attribute."""
    return has_attr(node, 'viewBox')


def get_position(node, attribute, fallback="0", absolute=False):
    """Retrieve value for node attribute passed as argument."""
    if attribute in ['x', 'y']:
        val, unit = parse_length(node.get(attribute, fallback), context='svg')
        if absolute and (val and not unit):
            unit = 'px'  # on root SVG, unitless is CSS px in outer context
        return (val, unit)


def get_dimension(node, attribute, fallback="100%", absolute=False):
    """Retrieve value for node attribute passed as argument."""
    if attribute in ['width', 'height']:
        value = node.get(attribute, None)
        value = fallback if value in [None, 'auto'] else value
        val, unit = parse_length(value, context='svg')
        if absolute and (val and not unit):
            unit = 'px'  # on root SVG, unitless is CSS px in outer context
        return (val, unit)


def get_viewbox(node):
    """Check and return viewBox of node if set."""
    attr = 'viewBox'
    viewbox = ()
    if attr in node.attrib:
        viewbox = parse_list_of_numbers(node.get(attr))
    if len(viewbox) != 4 or (viewbox[2] < 0 or viewbox[3] < 0):
        viewbox = None
    return viewbox


def get_aspectratio(node):
    """Get preserveAspectRatio from node."""
    align = "xMidYMid"
    clip = "meet"
    value = node.get('preserveAspectRatio', "{} {}".format(align, clip))
    props = value.split()
    if len(props) == 1:
        align, = props
    elif len(props) == 2:
        align, clip = props
    return {'align': align, 'clip': clip}


# ----- helper functions to retrieve viewport scale

def compute_dim_rel(svg_dim, vb_dim, fallback):
    """Compute a dimension based on relative unit identifier."""
    # medium font-size (em) in CSS px
    em_px = unit_to_unit(FONTSIZE_MEDIUM[0], FONTSIZE_MEDIUM[1], 'px')
    # fallback values
    if svg_dim is None:
        svg_dim = [100, '%']
    if vb_dim is None:
        vb_dim = fallback
    # dimension in CSS px based on svg size, viewBox, fallback
    if svg_dim[1] == '%':
        dim_px = (svg_dim[0] / 100.0) * vb_dim
    elif svg_dim[1] == 'em':
        dim_px = svg_dim[0] * em_px
    elif svg_dim[1] == 'ex':
        dim_px = svg_dim[0] * (em_px / 2.0)
    else:
        dim_px = fallback  # unsupported unit
    return dim_px


def compute_dim_px(svg_dim, vb_dim, fallback):
    """Compute px-based dimension."""
    # fallback values
    if svg_dim is None:
        svg_dim = [100, '%']
    # dimension in CSS px based on svg size, viewBox, fallback
    if svg_dim[1] in RELATIVE_UNITS:
        dim_px = compute_dim_rel(svg_dim, vb_dim, fallback)
    elif svg_dim[1] in UUCONV.keys():
        dim_px = svg_dim[0] * UUCONV[svg_dim[1]]
    elif svg_dim[1] in [None, '']:
        dim_px = svg_dim[0]
    else:
        dim_px = fallback  # unsupported unit
    return dim_px


# ----- functions to retrieve viewport scale

def svg_geometry(root):
    """Return width, height of SVGRoot."""
    svg_offset = [get_position(root, pos, absolute=True)
                  for pos in ['x', 'y']]
    svg_size = [get_dimension(root, dim, absolute=True)
                for dim in ['width', 'height']]
    return (tuple(svg_offset), tuple(svg_size))


def viewbox_geometry(root, svg_size=None):
    """Return verified values for viewBox rect of SVGRoot."""
    viewbox = get_viewbox(root)
    if viewbox is None:
        # compute a rect in CSS px based on svg or fallback size
        if svg_size is None:
            svg_size = svg_geometry(root)[1]
        vb_offset = [0, 0]
        vb_size = [compute_dim_px(svg_size[i], None, SIZE_FALLBACK[i][0])
                   for i in range(2)]
    else:
        # use values from existing viewBox
        vb_offset = viewbox[0:2]
        vb_size = viewbox[2:4]
    return (tuple(vb_offset), tuple(vb_size))


def viewport_geometry(root, svg_size=None, vb_size=None):
    """Return viewport rect in CSS px based on SVGRoot size and viewBox."""
    if svg_size is None:
        svg_size = svg_geometry(root)[1]
    if vb_size is None:
        vb_size = viewbox_geometry(root, svg_size)[1]
    # compute viewport size in CSS px
    vp_offset = [0.0, 0.0]
    vp_size = [compute_dim_px(svg_size[i], vb_size[i], SIZE_FALLBACK[i][0])
               for i in range(2)]
    return (tuple(vp_offset), tuple(vp_size))


def apply_aspectratio(root, svg_size=None, vb_size=None, vp_size=None):
    """Calculate viewport rect (uu), scale based on preserveAspectRatio."""
    if svg_size is None:
        svg_size = svg_geometry(root)[1]
    if vb_size is None:
        vb_size = viewbox_geometry(root, svg_size)[1]
    if vp_size is None:
        vp_size = viewport_geometry(root, svg_size, vb_size)[1]
    # default values
    offset = [0.0, 0.0]
    size = [1.0, 1.0]
    scale = [1.0, 1.0]
    aspect = {'align': "xMidYMid", 'clip': "meet"}
    align_factor = {'Min': 0.0, 'Mid': 0.5, 'Max': 1.0}
    # Compute scale as internally defined in src/document.cpp:606
    # Returns document scale as defined by width/height (in pixels) and
    # viewBox (real world to user-units).
    #
    # Use match_unit() to allow precision tolerance for physical unit
    # factor in viewport size (page width, height).
    scale = [dim and match_unit(vp_size[i] / dim, eps=1e-05) or scale[i]
             for i, dim in enumerate(vb_size)]
    # custom preserveAspectRatio applies only if viewBox attribute is present
    if has_viewbox(root):
        aspect.update(get_aspectratio(root))
    if aspect['align'] == 'none':
        # non-uniform scaling (not supported)
        u_scale = None
        # viewport size in user units
        size = [dim / factor for dim, factor in zip(vp_size, scale)]
    else:
        # uniform scaling, offset per axis
        u_scale = (aspect['clip'] == "meet" and min(*scale) or max(*scale))
        # viewport size in user units
        size = [dim / u_scale for dim in vp_size]
        # calculate offset in user units based on <align> parameter
        for i, dim in enumerate(vb_size):
            try:
                factor = align_factor[aspect['align'][(i*4)+1:(i+1)*4]]
            except KeyError:
                factor = 0.5
            val = factor * (dim - size[i])
            if not isclose(val, offset[i], abs_tol=1e-05):
                offset[i] = val
    # return computed offset, size, scale, uniform scale factor
    viewport = (tuple(offset), tuple(size))
    return (viewport, tuple(scale), u_scale)


def compute_viewport(root, geometry=True, scale=True):
    """Return geometry and/or scale of computed viewport."""
    viewport, vp_scale, uniform = apply_aspectratio(root)
    if geometry and scale:
        return (viewport, vp_scale, uniform)
    elif scale:
        return (vp_scale, uniform)
    elif geometry:
        return viewport


def compute_viewport_geom(root):
    """Return computed viewport rect in user units."""
    return compute_viewport(root, geometry=True, scale=False)


def compute_viewport_size(root):
    """Return computed viewport rect in user units."""
    return compute_viewport(root, geometry=True, scale=False)[1]


def compute_viewport_scale(root):
    """Return scale factors for user units."""
    scale, u_scale = compute_viewport(root, geometry=False, scale=True)
    return {'scale': scale, 'uniform': u_scale}


# ----- functions for unit conversion (based on scale of node root)

def check_root(node):
    """Check node, return root node of ElementTree object."""
    # pylint: disable=protected-access
    root = None
    if isinstance(node, etree._ElementTree):
        root = node.getroot()
    elif isinstance(node, etree._Element):
        root = node.getroottree().getroot()
    return root


def get_root_scale(node):
    """Return scale factor of document containing *node*.

    Calculate scale based on these SVGRoot attributes:
    'width', 'height', 'viewBox', 'preserveAspectRatio'
    """
    root = check_root(node)
    factor = UUCONV['px']
    if root is None:
        return factor
    else:
        viewport_props = compute_viewport_scale(root)
        if viewport_props['uniform'] is not None:
            # uniform scaling
            return viewport_props['uniform'] or factor
        elif len(viewport_props['scale']) == 2:
            # non-uniform scaling, return list with two scale factors
            # FIXME: as workaround, return scale for width for now
            return viewport_props['scale'][0]
        else:
            return factor


def get_root_offset(node):
    """Return offset of user to viewport coordinate system."""
    root = check_root(node)
    offset = (0.0, 0.0)
    if root is None:
        return offset
    else:
        vb_offset = viewbox_geometry(root)[0]
        vp_offset = compute_viewport_geom(root)[0]
        combined_offset = [vb_offset[i] + vp_offset[i] for i in range(2)]
        if len(combined_offset) == 2:
            return tuple(combined_offset)
        else:
            return offset


def get_root_size(node):
    """Return dimensions of root viewport in user units."""
    root = check_root(node)
    size = tuple(dim[0] for dim in SIZE_FALLBACK)
    if root is None:
        return size
    else:
        computed_size = compute_viewport_geom(root)[1]
        if len(computed_size) == 2:
            return computed_size
        else:
            return size


def mat_uu_to_unit(node, unit, scale=None):
    """Return matrix to scale user units to unit."""
    if scale is None:
        scale = get_root_scale(node)
    if unit in UUCONV.keys():
        sx = sy = scale / UUCONV[unit]
        return [[sx, 0.0, 0.0],
                [0.0, sy, 0.0]]
    else:
        return ident_mat()


def mat_unit_to_uu(node, unit, scale=None):
    """Return matrix to scale unit to user units."""
    if scale is None:
        scale = get_root_scale(node)
    if unit in UUCONV.keys():
        sx = sy = UUCONV[unit] / scale
        return [[sx, 0.0, 0.0],
                [0.0, sy, 0.0]]
    else:
        return ident_mat()


def mat_unit_to_unit(from_unit, to_unit):
    """Return matrix to scale *from_unit* to *to_unit*."""
    if from_unit in UUCONV.keys() and to_unit in UUCONV.keys():
        sx = sy = UUCONV[from_unit] / UUCONV[to_unit]
        return [[sx, 0.0, 0.0],
                [0.0, sy, 0.0]]
    else:
        return ident_mat()


def mat_uu_to_desktop(node):
    """Return matrix to transform user to desktop coordinates."""
    # pylint: disable=unused-variable
    root = check_root(node)
    # get offsets for viewBox and viewport
    vb_offset, vb_size = viewbox_geometry(root)
    vp_offset, vp_size = compute_viewport_geom(root)
    # flip vertically
    sx = + 1.0
    sy = - 1.0
    # translate by combined offsets and viewport height
    tx = - vb_offset[0] - vp_offset[0]
    ty = + vb_offset[1] + vp_offset[1] + vp_size[1]
    return [[sx, 0.0, tx],
            [0.0, sy, ty]]


def mat_desktop_to_uu(node):
    """Return matrix to transform desktop to user coordinates."""
    # pylint: disable=unused-variable
    root = check_root(node)
    # get offsets for viewBox and viewport
    vb_offset, vb_size = viewbox_geometry(root)
    vp_offset, vp_size = compute_viewport_geom(root)
    # flip vertically
    sx = + 1.0
    sy = - 1.0
    # translate by combined offsets and viewport height
    tx = + vb_offset[0] + vp_offset[0]
    ty = + vb_offset[1] + vp_offset[1] + vp_size[1]
    return [[sx, 0.0, tx],
            [0.0, sy, ty]]


def unit_to_uu(node, length, scale=None):
    """Return userunits given a string or list representation in units."""
    # pylint: disable=too-many-branches
    if scale is None:
        scale = get_root_scale(node)
    val = unit = None
    if length is not None:
        if isinstance(length, str):
            # parse length string
            val, unit = parse_length(length, 'abs')
        elif isinstance(length, (list, tuple)) and len(length) == 2:
            # split length list (val, unit)
            if is_real(length[0]):
                val = length[0]
            if isinstance(length[1], str):
                unit = length[1]
        elif is_real(length):
            # use real number
            val = length
        if val is None:
            val = 0.0
        if unit in UUCONV.keys():
            val *= (UUCONV[unit] / scale)
        elif unit in [None, '']:
            check = 'debug'
            if check == 'raise':
                # fail early
                raise UnsupportedUnitForLength("'{}'".format(unit))
            elif check == 'debug':
                # notify about case, pass
                debug("[unit_to_uu] uu to uu: {}".format(val))
            elif check == 'csspx':
                # assume unitless refers to absolute CSS px (ppi)
                val /= scale
            else:
                # target is same as source
                pass
        else:
            check = 'raise'
            if check == 'raise':
                # fail early
                raise UnsupportedUnitForLength("'{}'".format(unit))
            elif check == 'debug':
                # notify about case, pass
                debug("[unit_to_uu] unsupported unit: {}".format(val))
            else:
                # unsupported unit, return val
                pass
    return val


def uu_to_unit(node, val, unit, scale=None):
    """Return value in userunits converted to other units."""
    # pylint: disable=too-many-branches
    if scale is None:
        scale = get_root_scale(node)
    if val is not None:
        val = float(val)
        if isinstance(unit, str):
            unit = unit.strip()
        if unit in UUCONV.keys():
            val /= (UUCONV[unit] / scale)
        elif unit in [None, '']:
            check = 'debug'
            if check == 'raise':
                # fail early
                raise UnsupportedUnitForLength("'{}'".format(unit))
            elif check == 'debug':
                # notify about case, pass
                debug("[uu_to_unit] uu to uu: {}".format(val))
            elif check == 'csspx':
                # assume unitless refers to absolute CSS px (ppi)
                val *= scale
            else:
                # target is same as source
                pass
        else:
            check = 'raise'
            if check == 'raise':
                # fail early
                raise UnsupportedUnitForLength("'{}'".format(unit))
            elif check == 'debug':
                # notify about case, pass
                debug("[uu_to_unit] unsupported unit: {}".format(val))
            else:
                # unsupported unit, return val
                pass
    return val


def unit_to_unit(val, from_unit, to_unit):
    """Return value in *from_unit* converted to *to_unit*."""
    for unit in [from_unit, to_unit]:
        if unit not in UUCONV.keys():
            raise UnsupportedUnitForLength("'{}'".format(unit))
    return val * (UUCONV[from_unit] / UUCONV[to_unit])


def switch_uuconv(dpi='96'):
    """Switch base unit conversion table."""
    # pylint: disable=global-statement
    global UUCONV
    if dpi == '90':
        UUCONV = UUCONV_90DPI
    elif dpi == '96':
        UUCONV = UUCONV_96DPI
    else:  # unchanged
        pass


# ----- helper functions for transforms

def mat_apply_to_point(mat, point):
    """Apply matrix to point, return list with new coordinates."""
    return [mat[0][0]*point[0] + mat[0][1]*point[1] + mat[0][2],
            mat[1][0]*point[0] + mat[1][1]*point[1] + mat[1][2]]


# ----- helper functions for inkscape version

def str_to_int(val):
    """Return first int in string."""
    if isinstance(val, str):
        split_val = re.match(r'(\d*)(.*$)', val).groups()
        if len(split_val):
            return int(split_val[0])
        else:
            return 0


def version_from_string(version_str):
    """Read version from string ('.' as separator)."""
    dot_split = version_str.strip().split('.')
    version = []
    for substring in dot_split:
        version.append(str_to_int(substring))
    return version


def version_inside_range(version, major_min, minor_min, major_max, minor_max):
    """Compare version to major and minor range of versions."""
    if version[0] < major_min or version[0] > major_max:
        return False
    elif version[0] == major_min and version[1] <= minor_min:
        return False
    elif version[0] == major_max and version[1] >= minor_max:
        return False
    else:
        return True


# ----------------------------------------------------------------------------
# EffectUnits(): class, methods
# ----------------------------------------------------------------------------

class EffectUnits(EffectBase):
    """EffectBase sub-class prodiving doc scale for extension scripts."""

    def __init__(self):
        """Init base class."""
        showme("EffectUnits.__init__()")
        super(EffectUnits, self).__init__()

        # cached document scale
        self.cached_scale = None
        self.view_center = None

        # instance attributes
        self.inkruntime = [0, 0]
        self.inkversion = [0, 0]
        self.legacy = False

        # legacy mapping
        self.__uuconv = UUCONV

    # New method: get_document_scale()
    # ================================
    # The new method supports arbitrary uniform scale factors and is used in
    # the unit conversion methods inx_unit_to_uu() and inx_uu_to_unit().

    def get_document_scale(self, use_cache=True):
        """Return document scale factor.

        Calculate scale based on these SVGRoot attributes:
        'width', 'height', 'viewBox', 'preserveAspectRatio'
        """
        if self.cached_scale is None or not use_cache:
            self.cached_scale = get_root_scale(self.document)
        return self.cached_scale

    # Predefined transformations
    # ==========================
    # Methods to assist converting between coordinate systems or units.

    def mat_uu_to_unit(self, unit, rel='dt'):
        """Return matrix to scale user units to unit."""
        node = self.document.getroot()
        scale = None
        if rel == 'dt':
            scale = self.get_document_scale()
        elif rel == 'uu':
            scale = 1.0
        return mat_uu_to_unit(node, unit, scale)

    def mat_unit_to_uu(self, unit, rel='dt'):
        """Return matrix to scale unit to user units."""
        node = self.document.getroot()
        scale = None
        if rel == 'dt':
            scale = self.get_document_scale()
        elif rel == 'uu':
            scale = 1.0
        return mat_unit_to_uu(node, unit, scale)

    def mat_unit_to_unit(self, from_unit, to_unit):
        """Return matrix to scale *from_unit* to *to_unit*."""
        # pylint: disable=no-self-use
        return mat_unit_to_unit(from_unit, to_unit)

    def mat_uu_to_desktop(self):
        """Return matrix to transform user to desktop coordinates."""
        return mat_uu_to_desktop(self.document)

    def mat_desktop_to_uu(self):
        """Return matrix to transform desktop to user coordinates."""
        return mat_desktop_to_uu(self.document)

    # Unit conversion tools
    # =====================
    # Methods to assist unit handling in extensions / derived classes.
    #
    # These methods can be mapped to the original methods defined for
    # inkex.Effect(): unittouu(), uutounit()

    def inx_unit_to_uu(self, length, rel='dt'):
        """Return userunits given a string or list representation in units."""
        node = self.document.getroot()
        scale = None
        if rel == 'dt':
            scale = self.get_document_scale()
        elif rel == 'uu':
            scale = 1.0
        return unit_to_uu(node, length, scale)

    def inx_uu_to_unit(self, val, unit, rel='dt'):
        """Return value in userunits converted to other units."""
        node = self.document.getroot()
        scale = None
        if rel == 'dt':
            scale = self.get_document_scale()
        elif rel == 'uu':
            scale = 1.0
        return uu_to_unit(node, val, unit, scale)

    def inx_unit_to_unit(self, val, from_unit, to_unit):
        """Return value in *from_unit* converted to *to_unit*."""
        # pylint: disable=no-self-use
        return unit_to_unit(val, from_unit, to_unit)

    def inx_switch_uuconv(self, dpi='96'):
        """Allow extensions to override internal resolution."""
        # pylint: disable=global-statement
        global UUCONV
        if dpi == '90':
            UUCONV = UUCONV_90DPI
        elif dpi == '96':
            UUCONV = UUCONV_96DPI
        else:  # unchanged
            pass
        # update legacy instance attribute
        self.__uuconv = UUCONV
        self.cached_scale = None

    # Width, height, units, scale of page area and document
    # =====================================================
    # New convenience methods to retrieve specific document properties.
    #
    # Some of them can be mapped to methods defined in the original
    # inkex.Effect() base class:
    #   inx_get_viewport_prop("width")   -> getDocumentWidth()
    #   inx_get_viewport_prop("height")  -> getDocumentHeight()
    #   inx_get_viewbox_prop("unit")     -> getDocumentUnit()
    # They also can be used to remap addDocumentUnit().

    def inx_get_root(self, node=None):
        """Check node, return root node of ElementTree object."""
        # pylint: disable=protected-access
        root = None
        node = self.document if node is None else node
        if isinstance(node, etree._ElementTree):
            root = node.getroot()
        elif isinstance(node, etree._Element):
            root = node.getroottree().getroot()
        return root

    def inx_get_page_prop(self, prop="size"):
        """Return page property *prop*."""
        # pylint: disable=redefined-variable-type
        result = None
        root = self.inx_get_root()
        if prop == 'size':
            # Return page size as list of SVG lengths (as list).
            result = svg_geometry(root)[1]
        elif prop == 'width':
            # Return page width as SVG length (as list).
            result = svg_geometry(root)[1][0]
        elif prop == 'height':
            # Return page height as SVG length (as list).
            result = svg_geometry(root)[1][1]
        elif prop == 'units':
            # Return list with unit specifiers for page width, height.
            svg_width, svg_height = svg_geometry(root)[1]
            result = (svg_width[1], svg_height[1])
        return result

    def inx_get_viewport_prop(self, prop="size", unit=None):
        """Return viewport property *prop* (based on user units)."""
        # pylint: disable=redefined-variable-type
        result = None
        root = self.inx_get_root()
        if prop == 'size':
            # Return width, height of computed viewport area in user units.
            result = compute_viewport_size(root)
        elif prop == 'width':
            # Return width of computed viewport area in user units.
            result = compute_viewport_size(root)[0]
        elif prop == 'height':
            # Return height of computed viewport area in user units.
            result = compute_viewport_size(root)[1]
        elif prop == 'computed_width':
            # Return width of computed viewport area in CSS px or other unit.
            unit = unit or 'px'
            result = self.inx_uu_to_unit(compute_viewport_size(root)[0], unit)
        elif prop == 'computed_height':
            # Return width of computed viewport area in CSS px or other unit.
            unit = unit or 'px'
            result = self.inx_uu_to_unit(compute_viewport_size(root)[1], unit)
        elif prop == 'rect':
            # Return rect representing viewport in user coords, user units.
            width, height = compute_viewport_size(root)
            x, y = mat_apply_to_point(self.mat_desktop_to_uu(), [0.0, height])
            result = (x, y, width, height)
        elif prop == 'center':
            # Return center of viewport area in user coords, user units.
            result = mat_apply_to_point(
                self.mat_desktop_to_uu(),
                [(0.5 * v) for v in compute_viewport_size(root)])
        elif prop == 'scale':
            # Return scale factor of user units to real-world (CSS px).
            # (scale * real world = user unit)
            # (scale = user unit / real world)
            return 1.0 / self.get_document_scale()
        return result

    def inx_get_viewbox_prop(self, prop="size", unit=None):
        """Return document/viewbox property *prop*."""
        # pylint: disable=redefined-variable-type
        result = None
        root = self.inx_get_root()
        if prop == 'size':
            # Return width, height of computed viewBox area in user units.
            result = viewbox_geometry(root)[1]
        elif prop == 'width':
            # Return width of computed viewBox area in user units.
            result = viewbox_geometry(root)[1][0]
        elif prop == 'height':
            # Return height of computed viewBox area in user units.
            result = viewbox_geometry(root)[1][1]
        elif prop == 'computed_width':
            # Return width of computed viewBox area in CSS px or other unit.
            unit = unit or 'px'
            result = self.inx_uu_to_unit(viewbox_geometry(root)[1][0], unit)
        elif prop == 'computed_height':
            # Return width of computed viewBox area in CSS px or other unit.
            unit = unit or 'px'
            result = self.inx_uu_to_unit(viewbox_geometry(root)[1][1], unit)
        elif prop == 'rect':
            # Return rect representing viewBox in coords, units as requested
            unit = unit or 'uu'
            offset, size = viewbox_geometry(root)
            # user coords, uu or unknown unit
            pos = [offset[0], offset[1]]
            if unit == 'display':
                # desktop coords
                pos = mat_apply_to_point(
                    self.mat_uu_to_desktop(), [offset[0], offset[1] + size[1]])
                # display unit
                mat = self.mat_uu_to_unit(self.inx_get_display_prop("unit"))
                pos = mat_apply_to_point(mat, pos)
                size = mat_apply_to_point(mat, size)
            elif unit in UUCONV.keys():
                # desktop coords
                pos = mat_apply_to_point(
                    self.mat_uu_to_desktop(), [offset[0], offset[1] + size[1]])
                # other unit
                mat = self.mat_uu_to_unit(unit)
                pos = mat_apply_to_point(mat, pos)
                size = mat_apply_to_point(mat, size)
            # return x, y, width, height for rect
            result = (pos[0], pos[1], size[0], size[1])
        elif prop == 'unit':
            # Return dict with matching doc unit identifier and scale factor.
            scale = self.get_document_scale()
            eps = 0.01
            for key in UUCONV:
                if isclose(UUCONV[key], scale, rel_tol=eps):
                    return {'unit': key, 'factor': scale}
            return {'unit': 'custom', 'factor': scale}
        elif prop == 'scale':
            # Return scale factor of real world (CSS px) to user units.
            # (scale * user unit = real world)
            # (scale = real world / user unit)
            return self.get_document_scale()
        return result

    def inx_get_display_prop(self, prop="unit"):
        """Return display (desktop) property *prop*."""
        # pylint: disable=redefined-variable-type
        result = None
        root = self.inx_get_root()
        namedview = root.find(add_ns('namedview', 'sodipodi'))
        if prop == 'unit':
            # Return display units defined in namedview.
            unit = None
            if namedview is not None:
                unit = namedview.get(add_ns('document-units', 'inkscape'))
            result = unit or 'px'
        elif prop == 'center':
            # convert view center coords from namedview to user units, coords
            x = y = None
            if namedview is not None:
                x = namedview.get(add_ns('cx', 'inkscape'), None)
                y = namedview.get(add_ns('cy', 'inkscape'), None)
            try:
                x, y = [float(v) for v in (x, y)]
            except (TypeError, ValueError):
                result = self.inx_get_viewport_prop("center")
            else:
                result = mat_apply_to_point(
                    self.mat_desktop_to_uu(),
                    mat_apply_to_point(self.mat_unit_to_uu('px'), [x, y]))
            # update instance attribute
            self.view_center = list(result)
        return result

    # Inkscape version tools
    # ======================
    # Methods to assist detection of runtime and document inkscape version.

    def inx_inkscape_version(self, source="runtime", doc=None):
        """Check inkscape version information from runtime or document."""
        # pylint: disable=redefined-variable-type
        result = None
        if source == 'runtime':
            # Check runtime inkscape version information.
            #
            # TODO: implement function to access runtime inkscape version via
            # cli.  It will be based on the assumption that the running process
            # is the first 'inkscape' executable in $PATH (not safe!).
            #
            # NOTE: for now, rely on inkex features and pixels per inch (dpi)
            # to determine runtime inkscape version.  This requires that the
            # original unit conversion table (__uuconv) has not yet been
            # overloaded at this stage.
            #
            if hasattr(inkex_orig, 'unittouu'):
                self.inkruntime[1] = 48
            else:
                px_per_in = self.inx_unit_to_unit(1.0, 'in', 'px')
                if isclose(px_per_in, 90.0):
                    self.inkruntime[1] = 91
                elif isclose(px_per_in, 96.0):
                    self.inkruntime[1] = 92
            result = self.inkruntime
        elif source == 'document':
            # Check document's inkscape version information.
            #
            # NOTE: in Inkscape <= 0.91, the attribute 'inkscape:version' is
            # updated to the runtime version when the document is loaded. The
            # legacy status of these older versions (<= 0.91) with regard to
            # dpi is correct (90dpi).  With Inkscape >= 0.92, the version the
            # file was last edited / saved with gives a hint about the dpi
            # value originally used.  This original version is available as
            # long as the file has not been edited and saved again.  Inkscape
            # 0.92 and later updates the 'inkscape:version' attribute when
            # writing the file.
            #
            root = self.inx_get_root(doc)
            if root is not None:
                inkversion_attr = add_ns('version', 'inkscape')
                if inkversion_attr in root.attrib:
                    version = version_from_string(root.get(inkversion_attr))
                    legacy048 = version_inside_range(version, 0, 1, 0, 91)
                    legacy091 = version_inside_range(version, 0, 1, 0, 92)
                    self.inkversion = version[:2]
                    self.legacy = legacy048 or legacy091
            result = (self.inkversion, self.legacy)
        return result

    # Overload base methods
    # =====================
    # used to process the current document of the class instance

    def inx_main(self):
        """Method to apply changes to the document."""
        pass

    def inx_process(self, args=sys.argv[1:], output=True):
        """Affect an SVG document with a callback effect"""
        with self.inx_redirect_stderr():
            self.svg_file = args[-1]
            translite()
            self.inx_parse_svg()
            self.inx_get_display_prop("center")
            self.inx_main()
        if output:
            self.inx_output()


# ----------------------------------------------------------------------------
# EffectOptions(): class, methods
# ----------------------------------------------------------------------------

class EffectOptions(EffectBase):
    """EffectBase sub-class providing option parsing for extension scripts."""

    def __init__(self):
        """Init base class."""
        showme("EffectOptions.__init__()")
        super(EffectOptions, self).__init__()

        # instance attributes
        self.encoding = None
        self.options = None
        self.args = None
        self.selected = {}
        self.selected_nodes = {}

        # init option parser
        self.optparser = optparse.OptionParser(
            usage="usage: %prog [options] SVGfile", option_class=InkOption)

        # options provided by inkscape
        self.optparser.add_option("--id",
                                  action="append",
                                  type="string",
                                  dest="ids",
                                  default=[],
                                  help="id attribute of a selected object")
        self.optparser.add_option("--selected-nodes",
                                  action="append",
                                  type="string",
                                  dest="selected_nodes",
                                  default=[],
                                  help="id:subpath:pos of a selected node")

    def inx_get_input_encoding(self):
        """Get encoding for converting str (input) to Unicode (XML)."""
        self.encoding = str(sys.stdin.encoding)
        # Fallback for Windows (see bug #1518302)
        if self.encoding == 'cp0' or self.encoding is None:
            if locale.getpreferredencoding():
                self.encoding = str(locale.getpreferredencoding())
            else:
                self.encoding = 'UTF-8'

    def inx_to_unicode(self, msg, encoding=None):
        """Python 2: decode str (from input) to Unicode."""
        if encoding is None:
            encoding = self.encoding
        if sys.version_info < (3,):
            return msg.decode(encoding)
        else:
            return msg

    def to_unicode(self, msg, encoding=None):
        """Older name currently still in use in forked extensions."""
        # TODO: replace external usage with self.inx_to_unicode()
        return self.inx_to_unicode(msg, encoding)

    def inx_get_options(self, args=sys.argv[1:]):
        """Collect command line arguments."""
        self.options, self.args = self.optparser.parse_args(args)

    def inx_showme(self, msg=''):
        """Wrapper for debug output."""
        if VERBOSE or self.options.verbose:
            debug(msg)

    def inx_collect_ids(self, doc=None):
        """Iterate all elements, build dict of selected ids."""
        doc = self.document if doc is None else doc
        id_list = list(self.options.ids)
        for node in doc.getroot().iter(tag=etree.Element):
            if 'id' in node.attrib:
                node_id = node.get('id')
                if node_id in id_list:
                    self.selected[node_id] = node
                    id_list.remove(node_id)
                if not len(id_list):
                    break

    def inx_get_selected_nodes(self):
        """Collect selected nodes of selected SVG path elements."""
        # TODO: verify order of selected nodes as passed to the script by core
        # inkscape; use lists instead of dicts if or as needed.
        if hasattr(self.options, 'selected_nodes'):
            for path in self.options.selected_nodes:
                sel_data = path.rsplit(':', 2)
                path_id = sel_data[0]
                sub_path = int(sel_data[1])
                sel_node = int(sel_data[2])
                if path_id not in self.selected_nodes:
                    self.selected_nodes[path_id] = {sub_path: [sel_node]}
                else:
                    if sub_path not in self.selected_nodes[path_id]:
                        self.selected_nodes[path_id][sub_path] = [sel_node]
                    else:
                        self.selected_nodes[path_id][sub_path].extend(
                            [sel_node])
        else:
            self.selected_nodes = {}
            errormsg(
                "This version of Inkscape does not support processing " +
                "selected path nodes in script-based extensions.")

    def inx_main(self):
        """Method to apply changes to the document."""
        pass

    def inx_process(self, args=sys.argv[1:], output=True):
        """Affect an SVG document with a callback effect"""
        with self.inx_redirect_stderr():
            self.svg_file = args[-1]
            translite()
            self.inx_get_input_encoding()
            self.inx_get_options(args)
            self.inx_parse_svg()
            self.inx_collect_ids()
            self.inx_get_selected_nodes()
            self.inx_main()
        if output:
            self.inx_output()


# ----------------------------------------------------------------------------
# InxEffect(): class, methods
# ----------------------------------------------------------------------------

class InxEffect(EffectOptions, EffectUnits, EffectUtil):
    """Replacement for inkex.Effect() with updated document scale methods."""

    def __init__(self):
        """Init base class."""
        showme("InxEffect.__init__()")
        super(InxEffect, self).__init__()

    def inx_collect_ids(self, doc=None):
        """Iterate all elements, build id dicts (doc_ids, selected)."""
        doc = self.document if doc is None else doc
        id_list = list(self.options.ids)
        for node in doc.getroot().iter(tag=etree.Element):
            if 'id' in node.attrib:
                node_id = node.get('id')
                self.doc_ids[node_id] = 1
                if node_id in id_list:
                    self.selected[node_id] = node
                    id_list.remove(node_id)

    def inx_main(self):
        """Method to apply changes to the document."""
        pass

    def inx_process(self, args=sys.argv[1:], output=True):
        """Affect an SVG document with a callback effect"""
        with self.inx_redirect_stderr():
            self.svg_file = args[-1]
            translite()
            self.inx_get_input_encoding()
            self.inx_get_options(args)
            self.inx_parse_svg()
            self.inx_get_current_layer()
            self.inx_get_display_prop("center")
            self.inx_collect_ids()
            self.inx_get_selected_nodes()
            self.inx_main()
        if output:
            self.inx_output()


# ----------------------------------------------------------------------------
# Effect(): functions, class, methods
# ----------------------------------------------------------------------------

def localize():
    """Configure and install a gettext Translations instance."""
    return translite()


def addNS(tag, namespace=None):
    """Map addNS() to add_ns()."""
    # pylint: disable=invalid-name
    return add_ns(tag, namespace)


class Effect(InxEffect):
    """Compatibility class to map old function, class and method names."""
    # pylint: disable=invalid-name
    # pylint: disable=too-many-public-methods

    def __init__(self):
        """Init base class."""
        showme("Effect.__init__()")
        super(Effect, self).__init__()

        # instance attributes
        self.OptionParser = self.optparser

    # ----- methods most likely to be overloaded in class instances

    def parse(self, filename=None):
        """Map parse() to parent class method inx_parse_svg()."""
        return super(Effect, self).inx_parse_svg(filename)

    def inx_parse_svg(self, filename=None):
        """Map parse() to inx_parse_svg()."""
        return self.parse(filename)

    def affect(self, args=sys.argv[1:], output=True):
        """Map affect() to parent class method inx_process()."""
        return super(Effect, self).inx_process(args, output)

    def inx_process(self, args=sys.argv[1:], output=True):
        """Map affect() to inx_process()."""
        return self.affect(args, output)

    def effect(self):
        """Apply some effects on the document.  Extensions subclassing Effect
        must override this function and define the transformations in it."""
        return super(Effect, self).inx_main()

    def inx_main(self):
        """Method to apply changes to the document."""
        return self.effect()

    def output(self):
        """Map output() to parent class method inx_output()."""
        return super(Effect, self).inx_output()

    def inx_output(self):
        """Map output() to inx_output()."""
        return self.output()

    def getselected(self):
        """Map getselected() to parent class method inx_collect_ids()."""
        # FIXME: fix getselected() upstream in inkex.py.
        return super(Effect, self).inx_collect_ids()

    def inx_collect_ids(self):
        """Map getselected() to inx_collect_ids()."""
        return self.getselected()

    def uniqueId(self, old_id, make_new_id=True):
        """Map uniqueId() to parent class method inx_unique_id()."""
        return super(Effect, self).inx_unique_id(old_id, make_new_id)

    def inx_unique_id(self, old_id, make_new_id=True):
        """Map uniqueId() to inx_unique_id()."""
        return self.uniqueId(old_id, make_new_id)

    # ----- methods likely to be called only, not overloaded

    def getoptions(self, args=sys.argv[1:]):
        """Map getoptions() to inx_get_options()."""
        return self.inx_get_options(args)

    def getposinlayer(self):
        """Map getposinlayer() to get_pos_in_layer()."""
        return (self.inx_get_current_layer(),
                self.inx_get_display_prop("center"))

    def getElementById(self, id_):
        """Map getElementById() to inx_get_element_by_id()."""
        return self.inx_get_element_by_id(id_)

    def getParentNode(self, node):
        """Map getParentNode() to inx_get_parent()."""
        return self.inx_get_parent(node)

    def getdocids(self):
        """Map getdocids() to nothing."""
        # TODO: this method should be combined with getselected()
        pass

    def getNamedView(self):
        """Map getNamedView() to inx_get_namedview()."""
        return self.inx_get_namedview()

    def createGuide(self, pos_x, pos_y, angle):
        """Map createGuide() to inx_create_guide()."""
        # TODO: move current method to single extension which uses it
        # TODO: add module to add/edit guides with full unit support
        return self.inx_create_guide(pos_x, pos_y, angle)

    def xpathSingle(self, path):
        """Map xpathSingle() to inx_get_element_by_xpath()."""
        return self.inx_get_element_by_xpath(path)

    def getDocumentWidth(self):
        """Map getDocumentWidth() to computed viewport width in CSS px."""
        # NOTE: inx_get_page_width() does not resolve relative units
        # #return print_length(self.inx_get_page_width())
        # instead, return computed viewport width in CSS px as string
        return str(self.inx_get_viewport_prop("computed_width")) + 'px'

    def getDocumentHeight(self):
        """Map getDocumentHeight() to computed viewport height in CSS px."""
        # NOTE: inx_get_page_height() does not resolve relative units
        # #return print_length(self.inx_get_page_height())
        # instead, return computed viewport height in CSS px as string
        return str(self.inx_get_viewport_prop("computed_height")) + 'px'

    def getDocumentUnit(self):
        """Map getDocumentUnit() to inx_get_viewbox_prop('unit')."""
        # FIXME: this legacy method is misleading and needs to be removed.
        return self.inx_get_viewbox_prop("unit")['unit']

    def unittouu(self, string):
        """Map unittouu() to inx_unit_to_uu()."""
        return self.inx_unit_to_uu(string)

    def uutounit(self, val, unit):
        """Map uutounit() to inx_uu_to_unit()."""
        return self.inx_uu_to_unit(val, unit)

    def addDocumentUnit(self, value):
        """Map addDocumentUnit()."""
        # FIXME: this legacy method is misleading and needs to be removed.
        udict = self.inx_get_viewbox_prop("unit")
        if udict['unit'] != 'custom':
            return str(float(value)) + udict['unit']
        else:
            return str(float(value))


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
