#!/usr/bin/env python
"""
inkex_pyquery.py - evaluate or exec input string in current python context

Inspired by 'PyConsole' from WANG Longqi <http://www.pyconsole.tk/>, this
extension was written from scratch using base classes from inkex_local to
provide full Python 2/3 compatibility and extended output options (WIP).

Copyright (C) 2017 su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# standard library
import types
import re
import sys
import textwrap

# local library
try:
    import inkex_local as inkex
    # inkex.debug("inkex_local loaded.")
    from inkex_local import InxEffect
    # inkex.debug("InxEffect loaded from inkex_local.")
except ImportError:
    import inkex
    # inkex.debug("inkex loaded.")
    try:
        from inkex import InxEffect
        # inkex.debug("InxEffect loaded from inkex.")
    except ImportError:
        inkex.debug("This extension requires inkex_local.py.")
        sys.exit(1)
import simplestyle


def _ff(value, precision=8):
    """Pretty-printer for floats, return formatted string."""
    fstring = '.{0}f'.format(precision)
    return format(value, fstring).rstrip('0').rstrip('.')


def prpr(alist, precision=3):
    """Tiny pretty-printer for list of coords."""
    start = "("
    end = ")"
    sep = ", "
    result = start
    for num in alist:
        result += _ff(num, precision) + sep
    result = result[:-len(sep)] + end
    return result


def add_text(x=None, y=None, style=None):
    """Return text object <text/>."""
    text = inkex.etree.Element(inkex.addNS('text', 'svg'))
    text.set(inkex.addNS('space', 'xml'), "preserve")
    if style:
        text.set('style', simplestyle.formatStyle(style))
    if x:
        text.set('x', str(x))
    if y:
        text.set('y', str(y))
    return text


def add_line(line, x=None, y=None, dx=None, dy=None):
    """Return line wrapped in <tspan> element."""
    # pylint: disable=invalid-name
    tspan = inkex.etree.Element(inkex.addNS('tspan', 'svg'))
    tspan.set(inkex.addNS('role', 'sodipodi'), "line")
    if x:
        tspan.set('x', str(x))
    if dx:
        tspan.set('dx', str(dx))
    if y:
        tspan.set('y', str(y))
    if dy:
        tspan.set('dy', str(dy))
    tspan.text = str(line)
    return tspan


class PyQuery(InxEffect):
    """InxEffect-based class to directly query python expressions."""

    def __init__(self):
        """Init base class."""
        super(PyQuery, self).__init__()

        # input options
        self.optparser.add_option("--input_string",
                                  action="store", type="string",
                                  dest="input_string", default="",
                                  help="Input String")
        self.optparser.add_option("--query_mode",
                                  action="store", type="string",
                                  dest="query_mode", default="eval",
                                  help="Mode (eval or exec)")
        # format options
        self.optparser.add_option("--wrap",
                                  action="store", type="inkbool",
                                  dest="wrap", default=False,
                                  help="Wrap output")
        self.optparser.add_option("--header",
                                  action="store", type="inkbool",
                                  dest="header", default=False,
                                  help="Include input string in output")
        # output options
        self.optparser.add_option("--output_target",
                                  action="store", type="string",
                                  dest="output_target", default='stderr',
                                  help="Target for output string")
        # misc options
        self.optparser.add_option("--verbose",
                                  action="store", type="inkbool",
                                  dest="verbose", default=False,
                                  help="Verbose output (debugging):")

    def create_text(self, base=None, position=None, layer=None):
        """Configure and create new text object."""

        # base size (text, anchor offset)
        base = 12 if base is None else base
        # text anchor position relative to page area
        #   position: origin | right | center
        position = 'origin' if position is None else position
        # container element to append text to
        #   layer: new | current | None
        layer = 'current' if layer is None else layer

        # font properties
        font_size = self.inx_unit_to_uu("{}px".format(base))
        font_family = "monospace"
        # baseline spacing
        line_height = 1.25
        # text style
        sdict = {}
        sdict['font-size'] = font_size
        sdict['font-family'] = font_family
        sdict['line-height'] = line_height
        # text position
        if position == 'origin':
            x, y = inkex.mat_apply_to_point(
                self.mat_desktop_to_uu(),
                [0 + font_size, self.inx_get_viewport_prop('height')])
        elif position == 'right':
            x, y = inkex.mat_apply_to_point(
                self.mat_desktop_to_uu(),
                [self.inx_get_viewport_prop('width') + font_size,
                 self.inx_get_viewport_prop('height')])
        elif position == 'center':
            x, y = self.inx_get_display_prop('center')
        else:
            x, y = [0.0, 0.0]
        # create text
        text = add_text(x=x, y=y, style=sdict)
        # spacer line if text anchor at top of page
        if position in ['origin', 'right']:
            text.append(add_line(" "))
        # add to document
        if layer == 'new':
            parent = inkex.etree.Element(inkex.add_ns('g', 'svg'))
            parent.set(inkex.add_ns('groupmode', 'inkscape'), 'layer')
            parent.set(inkex.add_ns('label', 'inkscape'), 'docinfo')
            self.inx_get_root().append(parent)
        elif layer == 'current' and hasattr(self, 'current_layer'):
            parent = self.current_layer
        else:
            parent = self.inx_get_root()
        parent.append(text)
        # return new object
        return text

    def write_stderr(self, title, output):
        """Create text with output and write to stderr."""
        header = 'ruler' if self.options.header else None
        # add title string
        if header:
            inkex.errormsg(str(title))
            if header == 'blank':
                inkex.errormsg('')
            elif header == 'ruler':
                inkex.errormsg(len(title) * "=")
        # return output                     # output is a python object
        # inkex.debug(type(output))
        inkex.errormsg(str(output))         # errormsg() accepts single string

    def write_canvas(self, title, output):
        """Create text with output and add as SVG text to canvas."""
        # include title string as header + separator
        header = 'ruler' if self.options.header else None
        # create outer text object
        text = self.create_text(position="right", layer="current")
        # add input string
        if header:
            text.append(add_line(title))
            if header == 'blank':
                text.append(add_line(" "))
            elif header == 'ruler':
                text.append(add_line(len(title) * "="))
        # process output                    # output is a python object
        # inkex.debug(type(output))
        if self.options.wrap:
            # any object
            content = textwrap.wrap(str(output))    # list of strings
        else:
            if isinstance(output, dict):
                # dict object
                content = []                        # list of strings
                content.append('{')
                for key, item in output.items():
                    content.append("    '{}': {},".format(key, item))
                content.append('}')
            else:
                # other object
                content = str(output).splitlines()  # list of strings
        # fill text with content spans
        for line in content:                # content is list of strings
            text.append(add_line(line))     # add_line() applies str(line)

    def write_clipboard(self, title, output):
        """Create text with output and send to clipboard."""
        # pylint: disable=no-self-use,unused-argument
        inkex.errormsg("Output to clipboard is not yet implemented.")

    def write_file(self, title, output):
        """Create text with output and write to external file."""
        # pylint: disable=no-self-use,unused-argument
        inkex.errormsg("Output to file is not yet implemented.")

    def show(self, title=None, output=None, target='stderr'):
        """Output to requested targets."""
        if title is None and output is None:
            return
        if 'stderr' in target:
            result = self.write_stderr(title, output)
        if 'canvas' in target:
            result = self.write_canvas(title, output)
        if 'clipboard' in target:
            result = self.write_clipboard(title, output)
        if 'file' in target:
            result = self.write_file(title, output)
        return result

    def query(self, expression):
        """Return output from evaluating or executing expression."""
        # pylint: disable=eval-used,exec-used,bad-builtin
        if self.options.query_mode.startswith('eval'):
            return eval(expression)
        elif self.options.query_mode.startswith('exec'):
            ldict = locals()
            exec(expression)
            if 'output' in ldict.keys():
                return ldict['output']
            else:
                output = {}
                dropped = {}
                wlist = sorted(set(filter(None, re.split(r"\W+", expression))))
                for key in sorted(ldict.keys()):
                    item = ldict[key]
                    if key is not 'self' and key in wlist:
                        if (isinstance(item, types.ModuleType) or
                                isinstance(item, types.MethodType) or
                                isinstance(item, types.BuiltinMethodType) or
                                isinstance(item, types.BuiltinFunctionType)):
                            dropped[key] = item
                        else:
                            output[key] = item
                if len(dropped):
                    self.inx_showme("DEBUG - wordlist: " + str(wlist))
                    self.inx_showme("DEBUG - dropped: " + str(dropped))
                return output
        return None

    def inx_py_query(self, target='stderr'):
        """Eval or execute input string in python and return result."""
        if target is None:
            target = self.options.output_target
        expression = str(self.options.input_string)
        output = None
        if len(expression):
            # process input string
            output = self.query(expression)
        if output is not None:
            # output to target
            return self.show(expression, output, target)

    def inx_main(self):
        """Process an expression in current python context."""
        # pylint: disable=unused-variable
        result = self.inx_py_query(target=None)


if __name__ == '__main__':
    ME = PyQuery()
    ME.inx_process()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
