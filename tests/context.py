#!/usr/bin/env python
"""
Context file for unit tests
"""
# standard modules
import os
import sys


# local resources
CURRENT = os.path.dirname(__file__)

# script files
SRC_DIR = '../src'
LOCALSRC = os.path.abspath(os.path.join(CURRENT, SRC_DIR))

# test files
SVG_DIR = './svg'
SVGFILES = os.path.abspath(os.path.join(CURRENT, SVG_DIR))


# prepend path to local files
sys.path.insert(0, LOCALSRC)


# return abs path to test file 'file_name'
def test_file(name):
    return os.path.join(SVGFILES, name)


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
