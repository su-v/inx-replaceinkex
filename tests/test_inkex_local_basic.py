#!/usr/bin/env python
"""
Unit test file for ../src/inkex_local.py
"""
# pylint: disable=missing-docstring
# pylint: disable=invalid-name
# pylint: disable=no-self-use
# pylint: disable=unused-import

# standard modules
import unittest

# third-party modules
from lxml import etree

# local modules
from .context import test_file
from inkex_local import Effect
from inkex_local import EffectBase, EffectUtil, EffectUnits, EffectOptions
from inkex_local import InxEffect


class EffectClassesBasicTest(unittest.TestCase):
    """Test Effect() mapping and new base classes for InxEffect()."""

    def setUp(self):
        """Prepare test fixture."""
        pass

    # test Effect() mapping

    def test_1_effect_mapping_without_parameters(self):
        """Mapped Effect() class from original inkex.py."""
        args = [test_file('empty.svg')]
        test = Effect()
        test.affect(args, False)
        self.assertEqual(etree.tostring(test.document),
                         etree.tostring(test.original_document))

    # test custom derived classes

    def test_2_effectbase_without_parameters(self):
        """New base clase EffectBase()."""
        args = [test_file('empty.svg')]
        test = EffectBase()
        test.inx_process(args, False)
        self.assertFalse(test.inx_is_modified())

    def test_3_effectutil_without_parameters(self):
        """New derived base class EffectUtil()."""
        args = [test_file('empty.svg')]
        test = EffectUtil()
        test.inx_process(args, False)
        self.assertFalse(test.inx_is_modified())

    def test_4_effectunits_without_parameters(self):
        """New derived base class EffectUnits()."""
        args = [test_file('empty.svg')]
        test = EffectUnits()
        test.inx_process(args, False)
        self.assertFalse(test.inx_is_modified())

    def test_5_effectoptions_without_parameters(self):
        """New derived base class EffectOptions()."""
        args = [test_file('empty.svg')]
        test = EffectOptions()
        test.inx_process(args, False)
        self.assertFalse(test.inx_is_modified())

    def test_9_inxeffect_without_parameters(self):
        """Replacement for Effect(): InxEffect()."""
        args = [test_file('empty.svg')]
        test = InxEffect()
        test.inx_process(args, False)
        self.assertFalse(test.inx_is_modified())


if __name__ == '__main__':
    if 0:
        unittest.main()
    else:
        verbosity = 2
        testcases = [
            EffectClassesBasicTest,
        ]
        for testcase in testcases:
            suite = unittest.TestLoader().loadTestsFromTestCase(testcase)
            unittest.TextTestRunner(verbosity=verbosity).run(suite)
            print('')

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
