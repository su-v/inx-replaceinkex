#!/usr/bin/env python
"""
Unit test file for ../src/inkex_local.py
"""
# pylint: disable=missing-docstring
# pylint: disable=invalid-name
# pylint: disable=no-self-use
# pylint: disable=unused-import

# standard modules
import unittest

# third-party modules
from lxml import etree

# local modules
from . import context
import inkex_local as inkex


class inkexParseLengthTest(unittest.TestCase):
    """Test parse_length() function from inkex_local.py."""

    # parse length (context='all')

    def test_context_all_param_none(self):
        contxt = 'all'
        self.assertRaises(inkex.InvalidLengthString,
                          inkex.parse_length, None, contxt)

    def test_context_all_param_empty(self):
        contxt = 'all'
        # TODO: it this the right thing to do?
        self.assertTupleEqual(inkex.parse_length('', contxt), (0.0, ''))

    @unittest.expectedFailure
    def test_context_all_value_missing_digit(self):
        contxt = 'all'
        # FIXME: currently returns (0.0, '')
        self.assertTupleEqual(inkex.parse_length('.px', contxt), (0.0, 'px'))

    @unittest.expectedFailure
    def test_context_all_value_missing(self):
        contxt = 'all'
        # FIXME: currently returns (0.0, '')
        self.assertTupleEqual(inkex.parse_length('px', contxt), (0.0, 'px'))

    def test_context_all_unit_missing(self):
        contxt = 'all'
        self.assertTupleEqual(inkex.parse_length('1', contxt), (1.0, ''))

    def test_context_all_value_wrong_decimal_separator(self):
        contxt = 'all'
        self.assertRaises(inkex.InvalidLengthString,
                          inkex.parse_length, '0,1px', contxt)

    def test_context_all_value_partial_float_post(self):
        contxt = 'all'
        self.assertTupleEqual(inkex.parse_length('.1px', contxt),
                              (0.1, 'px'))

    def test_context_all_value_partial_float_pre(self):
        contxt = 'all'
        self.assertTupleEqual(inkex.parse_length('1.px', contxt),
                              (1.0, 'px'))

    def test_context_all_value_scientific_notation_lower(self):
        contxt = 'all'
        self.assertTupleEqual(inkex.parse_length('1e-03px', contxt),
                              (1e-03, 'px'))

    def test_context_all_value_scientific_notation_upper(self):
        contxt = 'all'
        self.assertTupleEqual(inkex.parse_length('1E-03mm', contxt),
                              (1e-03, 'mm'))

    def test_context_all_value_inf(self):
        contxt = 'all'
        self.assertEqual(repr(inkex.parse_length('inf', contxt)),
                         repr((float('inf'), '')))

    def test_context_all_value_nan(self):
        contxt = 'all'
        self.assertEqual(repr(inkex.parse_length('NaN', contxt)),
                         repr((float('nan'), '')))

    def test_context_all_unit_px(self):
        contxt = 'all'
        self.assertTupleEqual(inkex.parse_length('1px', contxt), (1.0, 'px'))

    def test_context_all_unit_km(self):
        contxt = 'all'
        self.assertTupleEqual(inkex.parse_length('1km', contxt), (1.0, 'km'))

    def test_context_all_unit_percent(self):
        contxt = 'all'
        self.assertTupleEqual(inkex.parse_length('1%', contxt), (1.0, '%'))

    def test_context_all_unit_unknown(self):
        contxt = 'all'
        self.assertRaises(inkex.UnsupportedUnitForLength,
                          inkex.parse_length, '9foo', contxt)

    # parse SVG length (context='svg')

    def test_context_svg_param_none(self):
        contxt = 'svg'
        # TODO: None as SVG length - check with SVG spec:
        # Should this return (100.0, '%')?
        self.assertRaises(inkex.InvalidLengthString,
                          inkex.parse_length, None, contxt)

    def test_context_svg_param_empty(self):
        contxt = 'svg'
        # TODO: empty string as SVG length - check with SVG spec:
        # Should this return (100.0, '%')?
        self.assertTupleEqual(inkex.parse_length('', contxt), (0.0, ''))

    def test_context_svg_unit_missing(self):
        contxt = 'svg'
        self.assertTupleEqual(inkex.parse_length('1', contxt), (1.0, ''))

    def test_context_svg_unit_px(self):
        contxt = 'svg'
        self.assertTupleEqual(inkex.parse_length('1px', contxt), (1.0, 'px'))

    def test_context_svg_unit_km(self):
        contxt = 'svg'
        self.assertRaises(inkex.UnsupportedUnitForLength,
                          inkex.parse_length, '1km', contxt)

    def test_context_svg_unit_percent(self):
        contxt = 'svg'
        self.assertTupleEqual(inkex.parse_length('1%', contxt), (1.0, '%'))

    def test_context_svg_unit_unknown(self):
        contxt = 'svg'
        self.assertRaises(inkex.UnsupportedUnitForLength,
                          inkex.parse_length, '9foo', contxt)

    # parse absolute lengths (uuconv.keys, 'px', '') (context='abs')

    def test_context_abs_param_none(self):
        contxt = 'abs'
        self.assertRaises(inkex.InvalidLengthString,
                          inkex.parse_length, None, contxt)

    def test_context_abs_param_empty(self):
        contxt = 'abs'
        # TODO: it this the right thing to do?
        self.assertTupleEqual(inkex.parse_length('', contxt), (0.0, ''))

    def test_context_abs_unit_missing(self):
        contxt = 'abs'
        self.assertTupleEqual(inkex.parse_length('1', contxt), (1.0, ''))

    def test_context_abs_unit_mm(self):
        contxt = 'abs'
        self.assertTupleEqual(inkex.parse_length('1mm', contxt), (1.0, 'mm'))

    def test_context_abs_unit_px(self):
        contxt = 'abs'
        self.assertTupleEqual(inkex.parse_length('1px', contxt), (1.0, 'px'))

    def test_context_abs_unit_km(self):
        contxt = 'abs'
        self.assertTupleEqual(inkex.parse_length('1km', contxt), (1.0, 'km'))

    def test_context_abs_unit_percent(self):
        contxt = 'abs'
        self.assertRaises(inkex.UnsupportedUnitForLength,
                          inkex.parse_length, '1%', contxt)

    # parse length supported by self.__uuconv (context='uuconv')

    def test_context_uuconv_param_none(self):
        contxt = 'uuconv'
        self.assertRaises(inkex.InvalidLengthString,
                          inkex.parse_length, None, contxt)

    def test_context_uuconv_param_empty(self):
        contxt = 'uuconv'
        self.assertRaises(inkex.UnsupportedUnitForLength,
                          inkex.parse_length, '', contxt)

    def test_context_uuconv_unit_missing(self):
        contxt = 'uuconv'
        self.assertRaises(inkex.UnsupportedUnitForLength,
                          inkex.parse_length, '1', contxt)

    def test_context_uuconv_unit_mm(self):
        contxt = 'uuconv'
        self.assertTupleEqual(inkex.parse_length('1mm', contxt), (1.0, 'mm'))

    def test_context_uuconv_unit_px(self):
        contxt = 'uuconv'
        self.assertTupleEqual(inkex.parse_length('1px', contxt), (1.0, 'px'))

    def test_context_uuconv_unit_km(self):
        contxt = 'uuconv'
        self.assertTupleEqual(inkex.parse_length('1km', contxt), (1.0, 'km'))

    def test_context_uuconv_unit_percent(self):
        contxt = 'uuconv'
        self.assertRaises(inkex.UnsupportedUnitForLength,
                          inkex.parse_length, '1%', contxt)


if __name__ == '__main__':
    if 0:
        unittest.main()
    else:
        verbosity = 2
        testcases = [
            inkexParseLengthTest,
        ]
        for testcase in testcases:
            suite = unittest.TestLoader().loadTestsFromTestCase(testcase)
            unittest.TextTestRunner(verbosity=verbosity).run(suite)
            print('')

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
