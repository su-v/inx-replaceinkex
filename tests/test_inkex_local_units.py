#!/usr/bin/env python
"""
Unit test file for ../src/inkex_local.py
"""
# pylint: disable=missing-docstring
# pylint: disable=invalid-name
# pylint: disable=no-self-use
# pylint: disable=unused-import

# standard modules
import unittest

# third-party modules
from lxml import etree

# local modules
from .context import test_file
from inkex_local import Effect
from inkex_local import EffectBase, EffectUtil, EffectUnits, EffectOptions
from inkex_local import InxEffect


class EffectUnitsBasicTest(unittest.TestCase):
    """Test EffectUnits() methods with Inkscape template files."""

    def setUp(self):
        """Prepare test fixture."""
        self.test = EffectUnits()
        # make sure to use 96dpi as unit conversion base by default
        self.test.inx_switch_uuconv('96')

    def test_effectunits_0_uuconv_switch(self):
        """EffectUnits.inx_switch_uuconv() method."""
        args = [test_file('empty.svg')]
        test = self.test
        test.inx_process(args, False)
        prec = 1e-05
        test.inx_switch_uuconv('90')
        self.assertAlmostEqual(
            test.inx_unit_to_unit(1, 'in', 'px'), 90.0, delta=prec)
        test.inx_switch_uuconv('96')
        self.assertAlmostEqual(
            test.inx_unit_to_unit(1, 'in', 'px'), 96.0, delta=prec)

    def test_effectunits_template_048_default(self):
        """EffectUnits() with default template of Inkscape 0.48."""
        args = [test_file('default-048.svg')]
        test = self.test
        test.inx_process(args, False)
        prec = 1e-05
        # with 90dpi
        test.inx_switch_uuconv('90')
        self.assertAlmostEqual(
            test.inx_unit_to_unit(1, 'in', 'px'), 90.0, delta=prec)
        self.assertTupleEqual(
            test.inx_get_page_prop('height'), (1052.3622047, 'px'))
        self.assertTupleEqual(
            test.inx_get_page_prop('units'), ('px', 'px'))
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('height'), 1052.3622047, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('height'), 1052.3622047, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.get_document_scale(), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1mm'), 3.54330709, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'mm'), 0.28222222, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(test.inx_get_viewport_prop('height'), 'mm'),
            297.0, delta=prec)
        # with 96dpi
        test.inx_switch_uuconv('96')
        self.assertAlmostEqual(
            test.inx_unit_to_unit(1, 'in', 'px'), 96.0, delta=prec)
        self.assertTupleEqual(
            test.inx_get_page_prop('height'), (1052.3622047, 'px'))
        self.assertTupleEqual(
            test.inx_get_page_prop('units'), ('px', 'px'))
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('height'), 1052.3622047, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('height'), 1052.3622047, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.get_document_scale(), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1mm'), 3.77952756, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'mm'), 0.26458333, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(test.inx_get_viewport_prop('height'), 'mm'),
            1052.3622047 * 0.26458333, delta=prec)

    def test_effectunits_template_048_page_height_90dpi(self):
        args = [test_file('default-048.svg')]
        test = self.test
        test.inx_process(args, False)
        prec = 1e-05
        test.inx_switch_uuconv('90')
        self.assertAlmostEqual(
            test.inx_uu_to_unit(test.inx_get_viewport_prop('height'), 'mm'),
            297.0, delta=prec)
        test.inx_switch_uuconv('96')

    @unittest.expectedFailure
    def test_effectunits_template_048_page_height_96dpi(self):
        args = [test_file('default-048.svg')]
        test = self.test
        test.inx_process(args, False)
        prec = 1e-05
        test.inx_switch_uuconv('96')
        self.assertAlmostEqual(
            test.inx_uu_to_unit(test.inx_get_viewport_prop('height'), 'mm'),
            297.0, delta=prec)

    def test_effectunits_template_091_default(self):
        """EffectUnits() with default template of Inkscape 0.91."""
        args = [test_file('default-091.svg')]
        test = self.test
        test.inx_process(args, False)
        prec = 1e-05
        # with 90dpi
        test.inx_switch_uuconv('90')
        self.assertAlmostEqual(
            test.inx_unit_to_unit(1, 'in', 'px'), 90.0, delta=prec)
        self.assertTupleEqual(
            test.inx_get_page_prop('height'), (297.0, 'mm'))
        self.assertTupleEqual(
            test.inx_get_page_prop('units'), ('mm', 'mm'))
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('height'), 1052.3622047, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('height'), 1052.3622047, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.get_document_scale(), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1mm'), 3.54330709, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'mm'), 0.28222222, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(test.inx_get_viewport_prop('height'), 'mm'),
            297.0, delta=prec)
        # with 96dpi
        test.inx_switch_uuconv('96')
        self.assertAlmostEqual(
            test.inx_unit_to_unit(1, 'in', 'px'), 96.0, delta=prec)
        self.assertTupleEqual(
            test.inx_get_page_prop('height'), (297.0, 'mm'))
        self.assertTupleEqual(
            test.inx_get_page_prop('units'), ('mm', 'mm'))
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('height'), 1052.3622047, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('scale'), 0.9375, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('height'), 1052.3622047, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('scale'), 1.06666667, delta=prec)
        self.assertAlmostEqual(
            test.get_document_scale(), 1.06666667, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1px'), 0.9375, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'px'), 1.06666667, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1mm'), 3.54330709, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'mm'), 0.28222222, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(test.inx_get_viewport_prop('height'), 'mm'),
            297.0, delta=prec)

    def test_effectunits_template_091_docscale_90dpi(self):
        args = [test_file('default-091.svg')]
        test = self.test
        test.inx_process(args, False)
        prec = 1e-05
        test.inx_switch_uuconv('90')
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'px'), 1.0, delta=prec)
        test.inx_switch_uuconv('96')

    @unittest.expectedFailure
    def test_effectunits_template_091_docscale_96dpi(self):
        args = [test_file('default-091.svg')]
        test = self.test
        test.inx_process(args, False)
        prec = 1e-05
        test.inx_switch_uuconv('96')
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'px'), 1.0, delta=prec)

    def test_effectunits_template_092_default(self):
        """EffectUnits() with default template of Inkscape 0.92."""
        args = [test_file('default-092.svg')]
        test = self.test
        test.inx_process(args, False)
        prec = 1e-05
        test.inx_switch_uuconv('96')
        self.assertAlmostEqual(
            test.inx_unit_to_unit(1, 'in', 'px'), 96.0, delta=prec)
        self.assertTupleEqual(
            test.inx_get_page_prop('height'), (297.0, 'mm'))
        self.assertTupleEqual(
            test.inx_get_page_prop('units'), ('mm', 'mm'))
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('height'), 297, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('scale'), 0.26458333, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('height'), 297, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('scale'), 3.77952756, delta=prec)
        self.assertAlmostEqual(
            test.get_document_scale(), 3.77952756, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1px'), 0.26458333, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'px'), 3.77952756, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1mm'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'mm'), 1.0, delta=prec)

    def test_effectunits_template_092_default_pt(self):
        """EffectUnits() with pt-based template of Inkscape 0.92."""
        args = [test_file('default_pt-092.svg')]
        test = self.test
        test.inx_process(args, False)
        prec = 1e-05
        test.inx_switch_uuconv('96')
        self.assertAlmostEqual(
            test.inx_unit_to_unit(1, 'in', 'px'), 96.0, delta=prec)
        self.assertTupleEqual(
            test.inx_get_page_prop('height'), (297.0, 'mm'))
        self.assertTupleEqual(
            test.inx_get_page_prop('units'), ('mm', 'mm'))
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('height'), 841.889764, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('scale'), 0.75, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('height'), 841.889764, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('scale'), 1.33333333, delta=prec)
        self.assertAlmostEqual(
            test.get_document_scale(), 1.33333333, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1px'), 0.75, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'px'), 1.33333333, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1pt'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'pt'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1mm'), 2.83464566, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'mm'), 0.35277778, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(test.inx_get_viewport_prop('height'), 'mm'),
            297.0, delta=prec)

    def test_effectunits_template_092_default_px(self):
        """EffectUnits() with px-based template of Inkscape 0.92."""
        args = [test_file('default_px-092.svg')]
        test = self.test
        test.inx_process(args, False)
        prec = 1e-05
        test.inx_switch_uuconv('96')
        self.assertAlmostEqual(
            test.inx_unit_to_unit(1, 'in', 'px'), 96.0, delta=prec)
        self.assertTupleEqual(
            test.inx_get_page_prop('height'), (297.0, 'mm'))
        self.assertTupleEqual(
            test.inx_get_page_prop('units'), ('mm', 'mm'))
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('height'), 1122.519685, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('height'), 1122.519685, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.get_document_scale(), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1mm'), 3.77952756, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'mm'), 0.26458333, delta=prec)

    def test_effectunits_empty_svg(self):
        """EffectUnits() with empty SVG document."""
        args = [test_file('empty.svg')]
        test = self.test
        test.inx_process(args, False)
        prec = 1e-05
        test.inx_switch_uuconv('96')
        self.assertAlmostEqual(
            test.inx_unit_to_unit(1, 'in', 'px'), 96.0, delta=prec)
        self.assertTupleEqual(
            test.inx_get_page_prop('height'), (100, '%'))
        self.assertTupleEqual(
            test.inx_get_page_prop('units'), ('%', '%'))
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('height'), 100, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewport_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('height'), 100, delta=prec)
        self.assertAlmostEqual(
            test.inx_get_viewbox_prop('scale'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.get_document_scale(), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'px'), 1.0, delta=prec)
        self.assertAlmostEqual(
            test.inx_unit_to_uu('1mm'), 3.77952756, delta=prec)
        self.assertAlmostEqual(
            test.inx_uu_to_unit(1, 'mm'), 0.26458333, delta=prec)


class EffectUnitsViewboxTest(unittest.TestCase):
    """Test EffectUnits() methods with viewBox and preserveAspectRatio."""

    def setUp(self):
        """Prepare test fixture."""
        self.test = EffectUnits()
        self.prec = 1e-05
        # make sure to use 96dpi as unit conversion base by default
        self.test.inx_switch_uuconv('96')

    def test_viewbox_viewport_in_uu(self):
        factor = 1.0
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300"
  height="150"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertEqual(
            self.test.get_document_scale(use_cache=False), factor)

    def test_viewport_missing_width(self):
        factor = 1.0
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  height="150"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertEqual(
            self.test.get_document_scale(use_cache=False), factor)
        self.assertEqual(
            self.test.inx_get_page_prop('width'), (100.0, '%'))
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('size'), (300.0, 150.0))
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'), (0.0, 0.0, 300.0, 150.0))

    def test_viewport_missing_height(self):
        factor = 1.0
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertEqual(
            self.test.get_document_scale(use_cache=False), factor)
        self.assertEqual(
            self.test.inx_get_page_prop('height'), (100.0, '%'))
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('size'), (300.0, 150.0))
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'), (0.0, 0.0, 300.0, 150.0))

    def test_viewport_missing_viewbox(self):
        factor = 1.0
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300"
  height="150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertEqual(
            self.test.get_document_scale(use_cache=False), factor)
        self.assertTupleEqual(
            self.test.inx_get_viewbox_prop('rect'), (0.0, 0.0, 300.0, 150.0))
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('size'), (300.0, 150.0))
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'), (0.0, 0.0, 300.0, 150.0))

    def test_viewbox_missing_viewport(self):
        factor = 1.0
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertEqual(
            self.test.get_document_scale(use_cache=False), factor)
        self.assertEqual(
            self.test.inx_get_page_prop('width'), (100.0, '%'))
        self.assertEqual(
            self.test.inx_get_page_prop('height'), (100.0, '%'))
        self.assertTupleEqual(
            self.test.inx_get_viewbox_prop('rect'), (0.0, 0.0, 300.0, 150.0))
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('size'), (300.0, 150.0))
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'), (0.0, 0.0, 300.0, 150.0))

    def test_viewbox_viewport_in_mm(self):
        factor = 1.0
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300mm"
  height="150mm"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertAlmostEqual(
            self.test.get_document_scale(use_cache=False),
            self.test.inx_unit_to_unit(1, 'mm', 'px') * factor,
            delta=self.prec)
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'),
            (0.0, 0.0, 300.0, 150.0))  # user coords, user units
        self.assertTupleEqual(
            self.test.inx_get_viewbox_prop('rect', 'mm'),
            (0.0, 0.0, 300.0, 150.0))  # desktop coords, mm

    def test_viewbox_viewport_in_pt(self):
        factor = 1.0
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300pt"
  height="150pt"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertAlmostEqual(
            self.test.get_document_scale(use_cache=False),
            self.test.inx_unit_to_unit(1, 'pt', 'px') * factor,
            delta=self.prec)
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'),
            (0.0, 0.0, 300.0, 150.0))  # user coords, user units
        self.assertTupleEqual(
            self.test.inx_get_viewbox_prop('rect', 'pt'),
            (0.0, 0.0, 300.0, 150.0))  # desktop coords, pt

    def test_viewbox_viewport_uniformly_scaled_down_in_mm(self):
        factor = 0.5
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300mm"
  height="150mm"
  viewBox="0 0 600 300"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertAlmostEqual(
            self.test.get_document_scale(use_cache=False),
            self.test.inx_unit_to_unit(1, 'mm', 'px') * factor,
            delta=self.prec)
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'),
            (0.0, 0.0, 600.0, 300.0))  # user coords, user units
        self.assertTupleEqual(
            self.test.inx_get_viewbox_prop('rect', 'mm'),
            (0.0, 0.0, 300.0, 150.0))  # desktop coords, mm

    def test_viewbox_viewport_uniformly_scaled_up_in_mm(self):
        factor = 2
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="600mm"
  height="300mm"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertAlmostEqual(
            self.test.get_document_scale(use_cache=False),
            self.test.inx_unit_to_unit(1, 'mm', 'px') * factor,
            delta=self.prec)
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'),
            (0.0, 0.0, 300.0, 150.0))  # user coords, user units
        self.assertTupleEqual(
            self.test.inx_get_viewbox_prop('rect', 'mm'),
            (0.0, 0.0, 600.0, 300.0))  # desktop coords, mm

    def test_viewbox_viewport_uniformly_scaled_down_in_pt(self):
        factor = 0.5
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300pt"
  height="150pt"
  viewBox="0 0 600 300"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertAlmostEqual(
            self.test.get_document_scale(use_cache=False),
            self.test.inx_unit_to_unit(1, 'pt', 'px') * factor,
            delta=self.prec)
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'),
            (0.0, 0.0, 600.0, 300.0))  # user coords, user units
        self.assertTupleEqual(
            self.test.inx_get_viewbox_prop('rect', 'pt'),
            (0.0, 0.0, 300.0, 150.0))  # desktop coords, pt

    def test_viewbox_viewport_uniformly_scaled_up_in_pt(self):
        factor = 2.0
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="600pt"
  height="300pt"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertAlmostEqual(
            self.test.get_document_scale(use_cache=False),
            self.test.inx_unit_to_unit(1, 'pt', 'px') * factor,
            delta=self.prec)
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'),
            (0.0, 0.0, 300.0, 150.0))  # user coords, user units
        self.assertTupleEqual(
            self.test.inx_get_viewbox_prop('rect', 'pt'),
            (0.0, 0.0, 600.0, 300.0))  # desktop coords, pt

    def test_viewbox_with_pos_offset_viewport_in_uu(self):
        factor = 1.0
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300"
  height="150"
  viewBox="100 50 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertEqual(
            self.test.get_document_scale(use_cache=False), factor)
        self.assertListEqual(
            self.test.inx_get_viewport_prop('center'),
            [250.0, 125.0])  # user coords, user units
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'),
            (100.0, 50.0, 300.0, 150.0))  # user coords, user units
        self.assertTupleEqual(
            self.test.inx_get_viewbox_prop('rect', 'display'),
            (0.0, 0.0, 300.0, 150.0))  # desktop coords, CSS px

    def test_viewbox_with_neg_offset_viewport_in_uu(self):
        factor = 1.0
        svg_string = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300"
  height="150"
  viewBox="-100 -50 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""
        self.test.document = etree.fromstring(svg_string).getroottree()
        self.assertEqual(
            self.test.get_document_scale(use_cache=False), factor)
        self.assertListEqual(
            self.test.inx_get_viewport_prop('center'),
            [50.0, 25.0])  # user coords, user units
        self.assertTupleEqual(
            self.test.inx_get_viewport_prop('rect'),
            (-100.0, -50.0, 300.0, 150.0))  # user coords, user units
        self.assertTupleEqual(
            self.test.inx_get_viewbox_prop('rect', 'display'),
            (0.0, 0.0, 300.0, 150.0))  # desktop coords, CSS px


if __name__ == '__main__':
    if 0:
        unittest.main()
    else:
        verbosity = 2
        testcases = [
            EffectUnitsBasicTest,
            EffectUnitsViewboxTest,
        ]
        for testcase in testcases:
            suite = unittest.TestLoader().loadTestsFromTestCase(testcase)
            unittest.TextTestRunner(verbosity=verbosity).run(suite)
            print('')

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
