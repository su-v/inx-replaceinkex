#!/usr/bin/env python
"""
Unit tests for unittouu(), uutounit() methods from inkex.Effect() class.

Reference:
    inkex.py: support arbitrary (but uniform) document scale (0.92)
    https://bugs.launchpad.net/inkscape/+bug/1508400

"""
# pylint: disable=missing-docstring
# pylint: disable=invalid-name

# standard library
import unittest
from lxml import etree

# local library
from . import context
from .upstream.inkex_trunk import Effect


SVG_MINIMAL = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg">
</svg>"""

SVG_PX_1 = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300px"
  height="150px"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""

SVG_PX_2 = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="600px"
  height="300px"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""

SVG_PX_3 = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300px"
  height="150px"
  viewBox="0 0 600 300"
  preserveAspectRatio="xMidYMid meet">
</svg>"""

SVG_MM_1 = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300mm"
  height="150mm"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""

SVG_MM_2 = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="600mm"
  height="300mm"
  viewBox="0 0 300 150"
  preserveAspectRatio="xMidYMid meet">
</svg>"""

SVG_MM_3 = b"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="300mm"
  height="150mm"
  viewBox="0 0 600 300"
  preserveAspectRatio="xMidYMid meet">
</svg>"""


class InkexEffectBasic(unittest.TestCase):

    def setUp(self):
        self.test = Effect()

    def tearDown(self):
        self.test = None

    def get_document(self, source=SVG_MINIMAL):
        svg = etree.fromstring(source)
        self.test.document = svg.getroottree()

    def test_uuconv_svg_px(self):
        self.get_document(SVG_PX_1)
        self.assertEqual(self.test.unittouu('1px'), 1.0)

    # known to fail (lp:1508400)
    @unittest.expectedFailure
    def test_uuconv_svg_px_upscaled(self):
        self.get_document(SVG_PX_2)
        self.assertEqual(self.test.unittouu('1px'), 0.5)

    # known to fail (lp:1508400)
    @unittest.expectedFailure
    def test_uuconv_svg_px_downscaled(self):
        self.get_document(SVG_PX_3)
        self.assertEqual(self.test.unittouu('1px'), 2.0)

    def test_uuconv_svg_mm(self):
        self.get_document(SVG_MM_1)
        self.assertEqual(self.test.unittouu('1mm'), 1.0)

    # known to fail (lp:1508400)
    @unittest.expectedFailure
    def test_uuconv_svg_mm_upscaled(self):
        self.get_document(SVG_MM_2)
        self.assertEqual(self.test.unittouu('1mm'), 0.5)

    # known to fail (lp:1508400)
    @unittest.expectedFailure
    def test_uuconv_svg_mm_downscaled(self):
        self.get_document(SVG_MM_3)
        self.assertEqual(self.test.unittouu('1mm'), 2.0)


class InkexEffectTemplateTest(unittest.TestCase):

    vp_width = 100          # viewport width
    vp_height = 100         # viewport height
    vp_w_unit = "px"        # viewport width unit
    vp_h_unit = "px"        # viewport height unit
    vb_width = 100          # viewBox width in uu
    vb_height = 100         # viewBox height in uu

    def doc_from_template(self, source=SVG_MINIMAL):
        svg = etree.fromstring(source)
        if hasattr(self, 'vp_width') and hasattr(self, 'vp_w_unit'):
            value = "{0}{1}".format(self.vp_width, self.vp_w_unit)
            svg.set('width', value)
        if hasattr(self, 'vp_height') and hasattr(self, 'vp_h_unit'):
            value = "{0}{1}".format(self.vp_height, self.vp_h_unit)
            svg.set('height', value)
        if hasattr(self, 'vb_width') and hasattr(self, 'vb_height'):
            value = "0 0 {0} {1}".format(self.vb_width, self.vb_height)
            svg.set('viewBox', value)
        return svg.getroottree()

    def setUp(self):
        self.prec = 1e-05
        self.test = Effect()
        self.test.document = self.doc_from_template(SVG_MINIMAL)

    def tearDown(self):
        self.test = None

    def test_1_unittouu_pageunit_to_userunit(self):
        computed = self.test.unittouu("1" + self.vp_w_unit)
        expected = self.vb_width / self.vp_width
        self.assertAlmostEqual(computed, expected, delta=self.prec)

    def test_2_uutounit_userunit_to_pageunit(self):
        computed = self.test.uutounit(1.0, self.vp_w_unit)
        expected = self.vp_width / self.vb_width
        self.assertAlmostEqual(computed, expected, delta=self.prec)

    def test_3_uutounit_vb_width_to_vp_width(self):
        computed = self.test.uutounit(self.vb_width, self.vp_w_unit)
        expected = self.vp_width
        self.assertAlmostEqual(computed, expected, delta=self.prec)

    def test_4_uutounit_vb_height_to_vp_height(self):
        computed = self.test.uutounit(self.vb_height, self.vp_h_unit)
        expected = self.vp_height
        self.assertAlmostEqual(computed, expected, delta=self.prec)

    def test_5_unittouu_vp_width_to_vb_width(self):
        computed = self.test.unittouu(str(self.vp_width) + self.vp_w_unit)
        expected = self.vb_width
        self.assertAlmostEqual(computed, expected, delta=self.prec)

    def test_6_unittouu_vp_height_to_vb_height(self):
        computed = self.test.unittouu(str(self.vp_height) + self.vp_h_unit)
        expected = self.vb_height
        self.assertAlmostEqual(computed, expected, delta=self.prec)


# known to fail (lp:1508400)
@unittest.expectedFailure
# NOTE: decorator on class fails with loadTestsFromTestCase() from Python 2.7
class InkexEffectDefault091px(InkexEffectTemplateTest):
    """Inkscape 0.91 template 'default.svg'."""
    vp_width = 210
    vp_height = 297
    vp_w_unit = "mm"
    vp_h_unit = "mm"
    vb_width = 744.09448819
    vb_height = 1052.3622047


class InkexEffectDefault092mm(InkexEffectTemplateTest):
    """Inkscape 0.92 template 'default.svg'."""
    vp_width = 210
    vp_height = 297
    vp_w_unit = "mm"
    vp_h_unit = "mm"
    vb_width = 210
    vb_height = 297


class InkexEffectDefault092px(InkexEffectTemplateTest):
    """Inkscape 0.92 template 'default_px.svg'."""
    vp_width = 210
    vp_height = 297
    vp_w_unit = "mm"
    vp_h_unit = "mm"
    vb_width = 793.7007874
    vb_height = 1122.519685


if __name__ == '__main__':
    if 0:
        unittest.main()
    else:
        verbosity = 2
        testcases = [
            InkexEffectBasic,
            InkexEffectTemplateTest,
            InkexEffectDefault091px,
            InkexEffectDefault092mm,
            InkexEffectDefault092px,
        ]
        for testcase in testcases:
            # NOTE: Python 2 unittest fails with 'TypeError' exception if class
            # instance has decorator 'unittest.expectedFailure'.  The class
            # decorator seems to work ok if run via discovery, or
            # loadTestsFromModule() (see run_tests.py), but not when running
            # the tests directly per module:
            #   python2 -m tests.test_inkex_upstream_docscale
            # The exception is not raised with Python 3 when run as module.
            suite = unittest.TestLoader().loadTestsFromTestCase(testcase)
            unittest.TextTestRunner(verbosity=verbosity).run(suite)
            print('')

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
