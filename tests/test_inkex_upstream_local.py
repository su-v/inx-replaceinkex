#!/usr/bin/env python
"""
Unit test file for Effect() mapping
"""
# pylint: disable=invalid-name
# pylint: disable=missing-docstring
# pylint: disable=unused-import

# standard library
import unittest

# local library
from .context import test_file
from .upstream.inkex_trunk import Effect
from inkex_local import Effect as EffectMapped


class EffectBasicTest(unittest.TestCase):
    """Test methods available in original Effect() class."""

    def setUp(self):
        """Prepare test fixture."""
        # inkex.py from Inkscape 0.92.1: 96dpi
        self.test = Effect()
        self.prec = 1e-05

    def test_inkex_048_default(self):
        args = [test_file('default-048.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'px')
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 1.0, delta=self.prec)

    @unittest.expectedFailure
    def test_inkex_091_default_docunit(self):
        args = [test_file('default-091.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'custom')

    @unittest.expectedFailure
    def test_inkex_091_default_px_to_uu(self):
        args = [test_file('default-091.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 90.0 / 96.0, delta=self.prec)

    @unittest.expectedFailure
    def test_inkex_091_default_uu_to_px(self):
        args = [test_file('default-091.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 96.0 / 90.0, delta=self.prec)

    @unittest.expectedFailure
    def test_inkex_091_default_mm_to_uu(self):
        args = [test_file('default-091.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.unittouu('1mm'), 3.54330709, delta=self.prec)

    @unittest.expectedFailure
    def test_inkex_091_default_uu_to_mm(self):
        args = [test_file('default-091.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'mm'), 0.28222222, delta=self.prec)

    def test_inkex_092_default(self):
        args = [test_file('default-092.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'mm')
        self.assertAlmostEqual(
            self.test.unittouu('1mm'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'mm'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 0.26458333, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 3.77952756, delta=self.prec)

    def test_inkex_092_default_pt(self):
        args = [test_file('default_pt-092.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'pt')
        self.assertAlmostEqual(
            self.test.unittouu('1pt'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'pt'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 0.75, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 1.33333333, delta=self.prec)

    def test_inkex_092_default_px(self):
        args = [test_file('default_px-092.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'px')
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.unittouu('1mm'), 3.77952756, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'mm'), 0.26458333, delta=self.prec)

    @unittest.expectedFailure
    def test_inkex_half_sized_1_docunit(self):
        args = [test_file('half_sized.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'custom')

    @unittest.expectedFailure
    def test_inkex_half_sized_1_px_to_uu(self):
        args = [test_file('half_sized.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 2.0, delta=self.prec)

    @unittest.expectedFailure
    def test_inkex_half_sized_1_uu_to_px(self):
        args = [test_file('half_sized.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 0.5, delta=self.prec)

    @unittest.expectedFailure
    def test_inkex_half_sized_1_uu_to_in(self):
        args = [test_file('half_sized.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(96.0, 'in'), 0.5 * 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(2.0 * 96.0, 'in'), 1.0, delta=self.prec)

    @unittest.expectedFailure
    def test_inkex_half_sized_2_docunit(self):
        args = [test_file('half_sized_mm.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'custom')

    @unittest.expectedFailure
    def test_inkex_half_sized_2_mm_to_uu(self):
        args = [test_file('half_sized_mm.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.unittouu('1mm'), 2.0, delta=self.prec)

    @unittest.expectedFailure
    def test_inkex_half_sized_2_uu_to_mm(self):
        args = [test_file('half_sized_mm.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'mm'), 0.5, delta=self.prec)

    @unittest.expectedFailure
    def test_inkex_half_sized_2_px_to_uu(self):
        args = [test_file('half_sized_mm.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 2.0 * 0.26458333, delta=self.prec)

    @unittest.expectedFailure
    def test_inkex_half_sized_2_uu_to_px(self):
        args = [test_file('half_sized_mm.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 0.5 * 3.77952756, delta=self.prec)


class EffectMappedBasicTest(unittest.TestCase):
    """Test methods available in mapped Effect() class in inkex_local.py."""

    def setUp(self):
        """Prepare test fixture."""
        # local inkex.py replacement: 96dpi
        self.test = EffectMapped()
        self.prec = 1e-05

    def test_inkex_local_048_default(self):
        args = [test_file('default-048.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'px')
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 1.0, delta=self.prec)

    def test_inkex_local_091_default_docunit(self):
        args = [test_file('default-091.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'custom')

    def test_inkex_local_091_default_px_to_uu(self):
        args = [test_file('default-091.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 90.0 / 96.0, delta=self.prec)

    def test_inkex_local_091_default_uu_to_px(self):
        args = [test_file('default-091.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 96.0 / 90.0, delta=self.prec)

    def test_inkex_local_091_default_mm_to_uu(self):
        args = [test_file('default-091.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.unittouu('1mm'), 3.54330709, delta=self.prec)

    def test_inkex_local_091_default_uu_to_mm(self):
        args = [test_file('default-091.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'mm'), 0.28222222, delta=self.prec)

    def test_inkex_local_092_default(self):
        args = [test_file('default-092.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'mm')
        self.assertAlmostEqual(
            self.test.unittouu('1mm'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'mm'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 0.26458333, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 3.77952756, delta=self.prec)

    def test_inkex_local_092_default_pt(self):
        args = [test_file('default_pt-092.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'pt')
        self.assertAlmostEqual(
            self.test.unittouu('1pt'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'pt'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 0.75, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 1.33333333, delta=self.prec)

    def test_inkex_local_092_default_px(self):
        args = [test_file('default_px-092.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'px')
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.unittouu('1mm'), 3.77952756, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'mm'), 0.26458333, delta=self.prec)

    def test_inkex_local_half_sized_1_docunit(self):
        args = [test_file('half_sized.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'custom')

    def test_inkex_local_half_sized_1_px_to_uu(self):
        args = [test_file('half_sized.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 2.0, delta=self.prec)

    def test_inkex_local_half_sized_1_uu_to_px(self):
        args = [test_file('half_sized.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 0.5, delta=self.prec)

    def test_inkex_local_half_sized_1_uu_to_in(self):
        args = [test_file('half_sized.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(96.0, 'in'), 0.5 * 1.0, delta=self.prec)
        self.assertAlmostEqual(
            self.test.uutounit(2.0 * 96.0, 'in'), 1.0, delta=self.prec)

    def test_inkex_local_half_sized_2_docunit(self):
        args = [test_file('half_sized_mm.svg')]
        self.test.affect(args, output=False)
        self.assertEqual(
            self.test.getDocumentUnit(), 'custom')

    def test_inkex_local_half_sized_2_mm_to_uu(self):
        args = [test_file('half_sized_mm.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.unittouu('1mm'), 2.0, delta=self.prec)

    def test_inkex_local_half_sized_2_uu_to_mm(self):
        args = [test_file('half_sized_mm.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'mm'), 0.5, delta=self.prec)

    def test_inkex_local_half_sized_2_px_to_uu(self):
        args = [test_file('half_sized_mm.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.unittouu('1px'), 2.0 * 0.26458333, delta=self.prec)

    def test_inkex_local_half_sized_2_uu_to_px(self):
        args = [test_file('half_sized_mm.svg')]
        self.test.affect(args, output=False)
        self.assertAlmostEqual(
            self.test.uutounit(1, 'px'), 0.5 * 3.77952756, delta=self.prec)


if __name__ == '__main__':
    if 0:
        unittest.main()
    else:
        verbosity = 2
        testcases = [
            EffectBasicTest,
            EffectMappedBasicTest,
        ]
        for testcase in testcases:
            suite = unittest.TestLoader().loadTestsFromTestCase(testcase)
            unittest.TextTestRunner(verbosity=verbosity).run(suite)
            print('')


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
